#include "EmgDataFilter.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <iterator>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

EmgDataFilter::EmgDataFilter(EmgDataContainer const &rData):
    m_rDataContainer(rData)
{

}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void EmgDataFilter::Update()
{
    return UpdateImpl();
}

EmgDataFilter::Container<EmgData>::size_type EmgDataFilter::From() const
{
    return std::distance(m_rDataContainer.begin(), begin());
}

EmgDataFilter::Container<EmgData>::size_type EmgDataFilter::To() const
{
    return std::distance(m_rDataContainer.begin(), end());
}

EmgDataFilter::Container<EmgData>::const_reference EmgDataFilter::operator[](size_t index) const
{
    return *(begin() + index);
}

EmgDataFilter::Container<EmgData>::const_iterator EmgDataFilter::begin() const
{
    return beginImpl();
}

EmgDataFilter::Container<EmgData>::const_iterator EmgDataFilter::end() const
{
    return endImpl();
}

EmgDataFilter::Container<EmgData>::size_type EmgDataFilter::size() const
{
    return std::distance(begin(), end());
}

bool EmgDataFilter::empty() const
{
    return (begin() == end());
}

} // namespace MyoVisualizer
