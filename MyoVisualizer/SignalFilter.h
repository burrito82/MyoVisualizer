#ifndef MYOVIS_SIGNALFILTER_H
#define MYOVIS_SIGNALFILTER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

template<typename TSignalFilter>
class SignalFilterIterator
{
public:
    SignalFilterIterator(TSignalFilter const &rSignalFilter, std::size_t iSensorId,
                         std::size_t iIndex = 0u):
        m_rSignalFilter(rSignalFilter),
        m_iSensorId{iSensorId},
        m_iIndex{iIndex}
    {

    }

    // InputIterator && ForwardIterator
    bool operator!=(SignalFilterIterator<TSignalFilter> const &rhs) const
    {
        return ((m_iSensorId != rhs.m_iSensorId)
                || (m_iIndex != rhs.m_iIndex));
    }

    typename TSignalFilter::value_type const &operator*() const
    {
        return m_rSignalFilter.GetValue(m_iSensorId, m_iIndex);
    }

    SignalFilterIterator<TSignalFilter> &operator++()
    {
        ++m_iIndex;
        return *this;
    }

    SignalFilterIterator<TSignalFilter> operator++(int)
    {
        auto oCopy = *this;
        ++m_iIndex;
        return oCopy;
    }

    std::size_t operator-(SignalFilterIterator<TSignalFilter> const &rhs)
    {
        return (m_iIndex - rhs.m_iIndex);
    }

    SignalFilterIterator<TSignalFilter> &operator+(std::size_t iOffset)
    {
        m_iIndex += iOffset;
        return *this;
    }

private:
    TSignalFilter const &m_rSignalFilter;
    std::size_t const m_iSensorId;
    std::size_t m_iIndex;
};

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
template<typename ContainerT>
class SignalFilter
{
public:
    using value_type = typename ContainerT::value_type;

    explicit SignalFilter(std::shared_ptr<SignalFilter<ContainerT>> shFilteredSource):
        m_aFilteredData(),
        m_apPlotCurve{},
        m_shFilteredSource{shFilteredSource}
    {
        for (int i = 0; i < 8; ++i)
        {
            m_apPlotCurve[i] = new QwtPlotCurve{};
            m_apPlotCurve[i]->setRenderHint(QwtPlotItem::RenderAntialiased, true);
            m_apPlotCurve[i]->setData(new QwtPointSeriesData{});
        }
        SetColor(Qt::blue);
    }

    SignalFilter():
        SignalFilter{nullptr}
    {
    }
    virtual ~SignalFilter() = default;

    std::size_t ValidBegin() const
    {
        std::size_t iValidBegin = ValidBeginImpl();
        if (m_shFilteredSource)
        {
            iValidBegin += m_shFilteredSource->ValidBegin();
        }
        return iValidBegin;
    }

    void SetData(ContainerT const &signalSource, std::size_t iSensorIndex)
    {
        if (!m_shFilteredSource)
        {
            m_aFilteredData[iSensorIndex].resize(signalSource.size());
            auto itDst = std::begin(m_aFilteredData[iSensorIndex]);
            for (size_t i = 0; i < signalSource.size(); ++i)
            {
                *itDst++ = Filter(signalSource, i, iSensorIndex);
            }
        }
        else
        {
            m_shFilteredSource->SetData(signalSource, iSensorIndex);
            std::size_t const iDataSize = m_shFilteredSource->m_aFilteredData[iSensorIndex].size();
            m_aFilteredData[iSensorIndex].resize(iDataSize);
            for (std::size_t i = 0u; i < iDataSize; ++i)
            {
                m_aFilteredData[iSensorIndex][i] = Filter(m_shFilteredSource->m_aFilteredData[iSensorIndex], i, iSensorIndex);
            }
        }
    }

    typename ContainerT::value_type const &GetValue(std::size_t iSensorId, std::size_t index) const
    {
        return m_aFilteredData[iSensorId][index];
    }

    SignalFilterIterator<SignalFilter<ContainerT>> GetBegin(std::size_t iSensorId) const
    {
        return SignalFilterIterator<SignalFilter<ContainerT>>{*this, iSensorId};
    }

    SignalFilterIterator<SignalFilter<ContainerT>> GetEnd(std::size_t iSensorId) const
    {
        return SignalFilterIterator<SignalFilter<ContainerT>>{*this, iSensorId, m_aFilteredData[iSensorId].size()};
    }

    void SetColor(QColor const &color)
    {
        for (int i = 0; i < 8; ++i)
        {
            auto oPen = m_apPlotCurve[i]->pen();
            oPen.setColor(color);
            oPen.setWidth(2);
            m_apPlotCurve[i]->setPen(oPen);
        }
    }

    QwtPlotCurve *GetPlotCurve(std::size_t iSensorId)
    {
        return m_apPlotCurve[iSensorId];
    }
protected:
    virtual value_type Filter(ContainerT const &in, size_t iElement, int iSensorId)
    {
        return Filter(in, iElement);
    }

private:
    virtual value_type Filter(ContainerT const &in, size_t iElement) = 0;
    virtual std::size_t ValidBeginImpl() const
    {
        return 0u;
    }

    ContainerT m_aFilteredData[8];
    QwtPlotCurve *m_apPlotCurve[8];
    std::shared_ptr<SignalFilter<ContainerT>> m_shFilteredSource;
};

} // namespace MyoVisualizer

#endif // ! MYOVIS_SIGNALFILTER_H

