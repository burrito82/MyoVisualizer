#ifndef MYOVIS_COMPUTATIONPLOT_H
#define MYOVIS_COMPUTATIONPLOT_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "qwt_plot.h"

#include <qpoint.h>

#include <array>
#include <cstdint>
#include <functional>
#include <memory>
#include <tuple>
#include <vector>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class QwtPlotCurve;
namespace MyoVisualizer
{
class IClassifier;
class EmgDataFilter;
template<typename ContainerT> class SignalFilter;
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
class ComputationPlot : public QwtPlot
{
    Q_OBJECT
public:
    using PlotData_t = std::vector<QPointF>;
    using PlotFunction_t = std::function<PlotData_t(PlotData_t const &)>;

    ComputationPlot(QWidget *parent = nullptr);

    void StartTimer(int iIntervalMs = 20);
    void StopTimer();

    void SetPlotCurve(QwtPlotCurve *pPlotCurve);

    std::uint64_t GetLatestTimestamp() const;
    float GetLatestValue() const;

    void AddSignalFilter(std::shared_ptr<SignalFilter<std::vector<QPointF>>> spSignalFilter);
    void AddPlotLine(QColor color, PlotFunction_t fpPlotFunction);
    void AddClassifier(QColor color, IClassifier *&&pClassifier);

signals:
    void on_value_changed(float fValue);
    void on_value_changed(QString strValue);
    void on_valueGradient_changed(float fGradient);
    void on_valueGradient_changed(QString strGradient);
    void on_variance_changed(float fVariance);
    void on_varianceGradient_changed(float fVarianceGradient);
    void on_statusMessage_changed(QString strStatusMessage);

protected:
    virtual void timerEvent(QTimerEvent *pEvent) override;

private:
    void Update();

    int m_iTimerId;
    std::vector<QwtPlotCurve *> m_vecPlotCurves;
    std::vector<std::shared_ptr<SignalFilter<std::vector<QPointF>>>> m_vecSignalFilters;
    std::vector<std::tuple<QwtPlotCurve *, PlotFunction_t>> m_vecPlotFunctions;
    std::uint64_t m_uiLatestTimestamp;
    float m_fLatestValue;
};

} // namespace MyoVisualizer

#endif // ! MYOVIS_COMPUTATIONPLOTVIEW_H

