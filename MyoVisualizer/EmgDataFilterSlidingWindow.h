#ifndef MYOVIS_EMGDATAFILTERSLIDINGWINDOW_H
#define MYOVIS_EMGDATAFILTERSLIDINGWINDOW_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "EmgDataFilter.h"
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
class EmgDataFilterSlidingWindow : public EmgDataFilter
{
public:
    EmgDataFilterSlidingWindow(EmgDataContainer const &rData);

    void SetBegin(size_t iBegin);
    size_t GetBegin() const;
    void SetWindowSize(size_t iWindowSize);
    size_t GetWindowSize() const;

private:
    virtual void UpdateImpl() override;
    virtual Container<EmgData>::const_iterator beginImpl() const override;
    virtual Container<EmgData>::const_iterator endImpl() const override;

    size_t m_iBegin;
    size_t m_iWindowSize;
    Container<EmgData>::const_iterator begin_;
    Container<EmgData>::const_iterator end_;
};

} // namespace MyoVisualizer

#endif // ! MYOVIS_EMGDATAFILTERSLIDINGWINDOW_H

