#ifndef MYOVISUALIZER_SENSORDATA_H
#define MYOVISUALIZER_SENSORDATA_H

#include <cstdint>
#include <mutex>

#include <qwt_series_data.h>

class SensorData : public QwtSeriesData<QPointF>
{
public:
    SensorData():
        m_vecData{},
        m_oBoundingRect{1.0, 1.0, -2.0, -2.0}
    {

    }
    virtual size_t size() const override
    {
        //std::lock_guard<std::mutex> oLock(m_oMutex);
        return m_vecData.size();
    }
    virtual QPointF sample(size_t i) const override
    {
        //std::lock_guard<std::mutex> oLock(m_oMutex);
        return m_vecData[i];
    }
    virtual QRectF boundingRect() const override
    {
        return m_oBoundingRect;
    }
    std::vector<QPointF> &Data()
    {
        return m_vecData;
    }
    void UpdateBoundingRect()
    {
        //std::lock_guard<std::mutex> oLock(m_oMutex);
        if (m_vecData.size() == 0u) return;
        m_oBoundingRect.setRect(m_vecData[0].x(), m_vecData[0].y(), 0.0, 0.0);
        for (QPointF const &p : m_vecData)
        {
            if (p.x() < m_oBoundingRect.left()) m_oBoundingRect.setLeft(p.x());
            if (p.x() > m_oBoundingRect.right()) m_oBoundingRect.setRight(p.x());
            if (p.y() < m_oBoundingRect.top()) m_oBoundingRect.setTop(p.y());
            if (p.y() > m_oBoundingRect.bottom()) m_oBoundingRect.setBottom(p.y());
        }
        m_oBoundingRect.setRect(0.0, 0.0, 2000.0, 1.0);
    }
    void UpdateSensorData(std::vector<int8_t> const &vecData)
    {
        //std::lock_guard<std::mutex> oLock(m_oMutex);
        m_vecData.clear();
        size_t iStart = 0u;
        size_t iEnd = vecData.size();
        if (vecData.size() >= 1000u)
        {
            iStart = vecData.size() - 1000u;
        }

        int x = 0;
        for (size_t i = iStart, x = 0; i < iEnd; ++i, ++x)
        {
            m_vecData.push_back(QPointF(x, vecData[i] % 255));
        }
    }
private:
    std::vector<QPointF> m_vecData;
    QRectF m_oBoundingRect;
    mutable std::mutex m_oMutex;
};

#endif // ! MYOVISUALIZER_SENSORDATA_H
