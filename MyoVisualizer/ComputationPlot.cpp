#include "ComputationPlot.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "Classifier.h"
#include "EmgDataFilter.h"
#include "Gesture.h"
#include "SignalFilter.h"

#include "ui_MyoVisualizer.h"

#include <qwt_plot.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <qwt_series_data.h>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
namespace
{

template<typename ContainerT>
class CPlotSeries : public QwtSeriesData<typename ContainerT::value_type>
{
    double const S_dMaxY = 128.0;
public:
    using value_type = typename ContainerT::value_type;
    CPlotSeries():
        m_vecData(),
        m_dMaxY{S_dMaxY}
    {
    }
    CPlotSeries(ContainerT const &cData):
        m_vecData(cData),
        m_dMaxY{S_dMaxY}
    {
    }

    template<typename IteratorT>
    void SetData(IteratorT const &begin, IteratorT const &end)
    {
        m_dMaxY = S_dMaxY;
        for (auto it = begin; it != end; ++it)
        {
            m_vecData.push_back(*it);
            m_dMaxY = std::max(m_dMaxY, it->y());
        }
    }

    template<typename AnotherContainerT>
    void SetData(AnotherContainerT const &cData)
    {
        m_dMaxY = S_dMaxY;
        for (auto p : cData)
        {
            m_vecData.push_back(p);
            m_dMaxY = std::max(m_dMaxY, p.y());
        }
    }
    virtual std::size_t size() const override
    {
        return m_vecData.size();
    }
    value_type sample(std::size_t i) const override
    {
        return m_vecData[i];
    }
    virtual QRectF boundingRect() const override
    {
        if (m_vecData.empty())
        {
            return QRectF{0.0, 0.0, static_cast<double>(size()), m_dMaxY};
        }
        else
        {
            auto iLeft = static_cast<float>(std::begin(m_vecData)->x());
            auto iWidth = static_cast<float>((std::end(m_vecData) - 1)->x()) - iLeft;
            return QRectF(iLeft, 0.0, iWidth, m_dMaxY);
        }
    }
private:
    ContainerT m_vecData;
    double m_dMaxY;
};

}
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
ComputationPlot::ComputationPlot(QWidget *parent):
    QwtPlot{parent},
    m_iTimerId{-1},
    m_vecPlotCurves{},
    m_uiLatestTimestamp{},
    m_fLatestValue{}
{
    setFooter("-");

    if (auto pCanvas = dynamic_cast<QwtPlotCanvas *>(canvas()))
    {
        pCanvas->setBorderRadius(10);
    }

    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->setPen(Qt::gray, 0.0, Qt::DotLine);
    grid->enableX(true);
    grid->enableXMin(true);
    grid->enableY(true);
    grid->enableYMin(false);
    grid->attach(this);
}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

namespace Computations
{

std::vector<QPointF> AverageOverSensors(std::shared_ptr<SignalFilter<std::vector<QPointF>>> const &rInput)
{
    std::vector<QPointF> vecAveraged{};
    // find valid boundary
    auto iEnd = rInput->GetEnd(0) - rInput->GetBegin(0);
    for (std::size_t iSensor = 1; iSensor < 8; ++iSensor)
    {
        iEnd = std::min(iEnd, rInput->GetEnd(iSensor) - rInput->GetBegin(iSensor));
    }
    vecAveraged.reserve(iEnd);
    // compute average
    for (auto iIndex = rInput->ValidBegin(); iIndex < iEnd; ++iIndex)
    {
        decltype(rInput->GetValue(0, 0).y()) dResultY{};
        for (std::size_t iSensor = 0; iSensor < 8; ++iSensor)
        {
            dResultY += rInput->GetValue(iSensor, iIndex).y();
        }
        //dResultY /= 8.0;
        vecAveraged.emplace_back(rInput->GetValue(0, iIndex).x(), dResultY);
    }
    return vecAveraged;
}

std::vector<QPointF> DetectSyringeHeuristic1(std::vector<QPointF> const &vecInput)
{
    std::vector<QPointF> vecResult{};
    vecResult.reserve(vecInput.size());

    return vecResult;
}

std::vector<QPointF> DetectSyringeHeuristic2(std::vector<QPointF> const &vecInput)
{
    std::vector<QPointF> vecResult{};
    vecResult.reserve(vecInput.size());

    return vecResult;
}

std::vector<QPointF> DetectSyringeHeuristic3(std::vector<QPointF> const &vecInput)
{
    std::vector<QPointF> vecResult{};
    vecResult.reserve(vecInput.size());

    return vecResult;
}

} // namespace Computations

void ComputationPlot::StartTimer(int iIntervalMs)
{
    if (m_iTimerId == -1)
    {
        m_iTimerId = startTimer(iIntervalMs);
    }
}

void ComputationPlot::StopTimer()
{
    if (m_iTimerId != -1)
    {
        killTimer(m_iTimerId);
        m_iTimerId = -1;
    }
}

void ComputationPlot::timerEvent(QTimerEvent *pEvent)
{
    Update();
}

void ComputationPlot::Update()
{
    if (!m_vecSignalFilters.empty())
    {
        for (std::size_t iPlot = 0u; iPlot < m_vecSignalFilters.size(); ++iPlot)
        {
            // data to plot
            QwtSeriesData<QPointF> *pSeries = new CPlotSeries<std::vector<QPointF>>{
                Computations::AverageOverSensors(m_vecSignalFilters[iPlot])
            };
            if (pSeries->size() > 1u && iPlot == 0u)
            {
                float fValue = pSeries->sample(pSeries->size() - 1u).y();
                m_fLatestValue = fValue;
                m_uiLatestTimestamp = static_cast<std::uint64_t>(pSeries->sample(pSeries->size() - 1u).x() * 1000.0f);
                emit on_value_changed(QString::fromStdString(std::string{"Value: " + std::to_string(fValue)}));

                float fGradient = (pSeries->sample(pSeries->size() - 1u).y() - pSeries->sample(pSeries->size() - 2u).y())
                    / (pSeries->sample(pSeries->size() - 1u).x() - pSeries->sample(pSeries->size() - 2u).x());
                emit on_valueGradient_changed(QString::fromStdString(std::string{"Gradient: " + std::to_string(fGradient)}));
            }
            m_vecPlotCurves[iPlot]->setData(pSeries);
        }
        // Plot functions only computed for the avg of the first signal filter
        for (auto &oTuple : m_vecPlotFunctions)
        {
            auto plot = std::get<0>(oTuple);
            auto fp = std::get<1>(oTuple);
            QwtSeriesData<QPointF> *pSeries = new CPlotSeries<std::vector<QPointF>>{
                fp(Computations::AverageOverSensors(m_vecSignalFilters[0u]))
            };
            double dAxisScale = 250.0;
            double dDiff = pSeries->boundingRect().right() - pSeries->boundingRect().left();
            if (dDiff > 1000.0)
            {
                dAxisScale = 1000.0;
            }
            if (dDiff > 5000.0)
            {
                dAxisScale = 5000.0;
            }
            setAxisScale(xBottom, pSeries->boundingRect().left(), pSeries->boundingRect().right(), dAxisScale);
            plot->setData(pSeries);
        }
    }
    replot();
}

std::uint64_t ComputationPlot::GetLatestTimestamp() const
{
    return m_uiLatestTimestamp;
}

float ComputationPlot::GetLatestValue() const
{
    return m_fLatestValue;
}

void ComputationPlot::AddSignalFilter(std::shared_ptr<SignalFilter<std::vector<QPointF>>> spSignalFilter)
{
    m_vecSignalFilters.push_back(spSignalFilter);

    auto pPlotCurve = new QwtPlotCurve{};
    pPlotCurve->setRenderHint(QwtPlotItem::RenderAntialiased, true);
    pPlotCurve->setData(new QwtPointSeriesData{});
    pPlotCurve->attach(this);
    pPlotCurve->setPen(spSignalFilter->GetPlotCurve(0)->pen());
    m_vecPlotCurves.push_back(pPlotCurve);
}

void ComputationPlot::AddPlotLine(QColor color, std::function<std::vector<QPointF>(std::vector<QPointF> const &)> fpPlotFunction)
{
    auto pPlotCurve = new QwtPlotCurve{};
    pPlotCurve->setRenderHint(QwtPlotItem::RenderAntialiased, true);
    pPlotCurve->setData(new QwtPointSeriesData{});
    pPlotCurve->attach(this);
    auto oPen = pPlotCurve->pen();
    oPen.setColor(color);
    oPen.setWidth(2);
    pPlotCurve->setPen(oPen);

    m_vecPlotFunctions.push_back(std::make_tuple(pPlotCurve, fpPlotFunction));
}

void ComputationPlot::AddClassifier(QColor color, IClassifier *&&pClassifier)
{
    static double classifierHeight = 140.0;
    classifierHeight -= 10.0f;
    auto const height = classifierHeight;
    AddPlotLine(color, [pClassifier, height](ComputationPlot::PlotData_t const &vecInput)
    {
        auto const height = classifierHeight;
        auto gestures = pClassifier->Classify(vecInput);
        ComputationPlot::PlotData_t vecPlot{};
        ComputationPlot::PlotData_t::value_type newPoint{};
        if (vecInput.empty())
        {
            return vecPlot;
        }

        Gesture::GestureId id = '1';
        auto const firstX = static_cast<std::uint64_t>(vecInput[0].x());
        auto const lastX = static_cast<std::uint64_t>(vecInput[vecInput.size() - 1].x());

        if (gestures.empty())
        {
            newPoint.setX(firstX);
            newPoint.setY(0.0);
            vecPlot.push_back(newPoint);
            newPoint.setX(lastX);
            newPoint.setY(0.0);
            vecPlot.push_back(newPoint);
            return vecPlot;
        }

        auto it = std::begin(gestures);
        if (it != std::end(gestures) && it->Start() < firstX)
        {
            newPoint.setX(firstX);
            newPoint.setY(height);
            vecPlot.push_back(newPoint);

            newPoint.setX(it->End());
            newPoint.setY(height);
            vecPlot.push_back(newPoint);

            newPoint.setX(it->End() + 1.0);
            newPoint.setY(0.0);
            vecPlot.push_back(newPoint);
            gestures.erase(it);
        }
        else
        {
            newPoint.setX(firstX);
            newPoint.setY(0.0);
            vecPlot.push_back(newPoint);
        }

        for (it = std::begin(gestures);
             it != std::end(gestures);
             ++it)
        {
            newPoint.setX(it->Start() - 1.0);
            newPoint.setY(0.0);
            vecPlot.push_back(newPoint);

            newPoint.setX(it->Start());
            newPoint.setY(height);
            vecPlot.push_back(newPoint);

            newPoint.setX(it->End());
            newPoint.setY(height);
            vecPlot.push_back(newPoint);

            newPoint.setX(it->End() + 1.0);
            newPoint.setY(0.0);
            vecPlot.push_back(newPoint);
        }

        {
            newPoint.setX(lastX);
            newPoint.setY(0.0);
            vecPlot.push_back(newPoint);
        }

        return vecPlot;
    });
}

} // namespace MyoVisualizer
