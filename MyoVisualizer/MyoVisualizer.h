#ifndef MYOVIS_MYOVISUALIZER_H
#define MYOVIS_MYOVISUALIZER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "GestureRecorder.h"
#include "Gyroscope.h"
#include "MyoControl.h"
#include "MyoSyringe.h"

#include <QtWidgets/QMainWindow>
#include "ui_MyoVisualizer.h"

#include <array>
#include <functional>
#include <memory>
#include <mutex>
#include <vector>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/
/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
class IClassifier;
class MyoListener;
class MyoPlot;
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/
class MyoVisualizer : public QMainWindow
{
    Q_OBJECT

public:
    explicit MyoVisualizer(QWidget *parent = nullptr);
    ~MyoVisualizer();

public slots:
    void on_btn0_clicked();
    void on_btn1_clicked();
    void on_btn2_clicked();
    void on_btn3_clicked();
    void on_btn4_clicked();
    void on_btn5_clicked();
    void on_btn6_clicked();
    void on_btn7_clicked();
    void on_actionConnect_triggered();
    void on_actionDisconnect_triggered();
    void on_actionLoad_EMG_data_from_csv_triggered();
    void on_actionSave_to_csv_triggered();
    void on_actionClear_triggered();
    void on_actionLoad_Gesture_data_triggered();
    void on_actionExport_All_triggered();
    void on_actionUpdate_Window_triggered();
    void on_windowSlider_sliderMoved();
    void on_windowSizeSlider_sliderMoved();
    void on_windowSizeSbox_valueChanged();
    void on_spSyringeVolume_valueChanged();
    void on_spAdmissionRate_valueChanged();
    void on_cbLowPass_stateChanged();
    void on_cbHighPass_stateChanged();
    void on_dbSbLowPass_valueChanged();
    void on_dbSbHighPass_valueChanged();

    void on_EmgDataAdded();

    void timerEvent(QTimerEvent *pEvent);

protected:
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

private:
    void StartTimer();
    void StopTimer();
    void GuiThread();
    void RunInGuiThread(std::function<void()> func);

    void Show(size_t iSensor);

    Ui::MyoVisualizerClass ui;
    MyoControl m_oMyoControl;
    std::shared_ptr<MyoListener> m_spListener;
    std::shared_ptr<EmgDataFilter> m_spViewFilter;
    std::array<MyoPlot *, 8> m_aMyoPlots;
    std::array<QPushButton *, 8> m_aButtons;
    GestureRecorder m_oGestureRecorder;
    IClassifier *m_pClassifier;
    Gyroscope m_oGyroscope;
    MyoSyringe m_oMyoSyringe;
    std::mutex m_oGuiFuncsMutex;
    int m_iTimerId;
    std::vector<std::function<void()>> m_vecRunInGuiThread;

    class Impl;
    std::unique_ptr<Impl> m_pImpl;
};

} // namespace MyoVisualizer

#endif // ! MYOVIS_MYOVISUALIZER_H
