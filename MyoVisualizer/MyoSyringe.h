#ifndef MYOVIS_MYOSYRINGE_H
#define MYOVIS_MYOSYRINGE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <cstdint>
#include <functional>
#include <iosfwd>
#include <tuple>
#include <utility>
#include <vector>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
class Gyroscope;
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
class MyoSyringe
{
public:
    enum SyringeState
    {
        WAITING,
        ADMINISTRATION_IN_PROGRESS
    };
    MyoSyringe(Gyroscope const &oGyroscope);

    void SetCurrentEmg(float fValue, std::uint64_t uiTimestamp);

    SyringeState GetState() const;
    std::uint64_t GetSyringeBegin() const;
    std::uint64_t GetNow() const;
    std::uint64_t GetSyringeEnd() const;

    void SetOnSyringeBegin(std::function<void()> funcOnSyringeBegin);
    void SetOnSyringeEnd(std::function<void(std::uint64_t uiAdmissionDuration)> funcOnSyringeEnd);

    void Export(std::ostream &os) const;
private:
    void LogToHistory(std::uint64_t uiTimestamp);

    Gyroscope const &m_oGyroscope;
    float m_fLastValue;
    float m_fValueAverage;
    float m_fWaitingValueAverage;
    SyringeState m_eState;
    std::function<void()> m_funcOnSyringeBegin;
    std::function<void(std::uint64_t uiAdmissionDuration)> m_funcOnSyringeEnd;
    std::uint64_t m_uiSyringeBegin;
    std::uint64_t m_uiNow;
    std::uint64_t m_uiSyringeEnd;
    float m_fMaxAdmValue;
    std::vector<std::pair<std::uint64_t, SyringeState>> m_vecHistory;
};

std::ostream &operator<<(std::ostream &os, MyoSyringe const &gyro);

} // namespace MyoVisualizer

#endif // ! MYOVIS_MYOSYRINGE_H

