#include "Gesture.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
Gesture::Gesture():
    m_gestureId{},
    m_uiStartTime{},
    m_uiEndTime{}
{

}

Gesture::Gesture(GestureId const &gestureId, std::uint64_t uiStartTime):
    m_gestureId(gestureId),
    m_uiStartTime{uiStartTime},
    m_uiEndTime{}
{

}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
Gesture Gesture::Construct(std::uint64_t uiStartTime,
                                  std::uint64_t uiEndTime, 
                                  Gesture::GestureId gestureId)
{
    Gesture gesture{};
    gesture.m_uiStartTime = uiStartTime;
    gesture.m_uiEndTime = uiEndTime;
    gesture.m_gestureId = gestureId;
    return gesture;
}

void Gesture::RecordEnd(std::uint64_t uiEndTime)
{
    m_uiEndTime = uiEndTime;
}

std::uint64_t Gesture::Start() const
{
    return m_uiStartTime;
}

std::uint64_t Gesture::End() const
{
    return m_uiEndTime;
}

Gesture::GestureId Gesture::Id() const
{
    return m_gestureId;
}

} // namespace MyoVisualizer
