#ifndef MYOVIS_EMGDATACONTAINER_H
#define MYOVIS_EMGDATACONTAINER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "EmgContainer.h"
#include "EmgData.h"

#define BOOST_DISABLE_ASSERTS 1
#include <boost/container/stable_vector.hpp>

#include <QObject>

#include <algorithm>
#include <ostream>
#include <list>
#include <mutex>
#include <deque>
#include <vector>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
class EmgDataContainer : public QObject
{
    Q_OBJECT

    //template<typename T> using Container = boost::container::stable_vector<T>;
    template<typename T> using Container = EmgContainer<T>;
public:
    EmgDataContainer();

    void Add(EmgData const &rEmg);

    Container<EmgData>::const_reference operator[](size_t index) const
    {
        return m_vecData[index];
    }
    Container<EmgData>::reference operator[](size_t index)
    {
        return m_vecData[index];
    }
    Container<EmgData>::reference front()
    {
        return *begin();
    }
    Container<EmgData>::const_reference front() const
    {
        return *begin();
    }
    Container<EmgData>::reference back()
    {
        return *(end() - 1);
    }
    Container<EmgData>::const_reference back() const
    {
        return *(end() - 1);
    }
    Container<EmgData>::const_iterator begin() const
    {
        return std::begin(m_vecData);
    }
    Container<EmgData>::iterator begin()
    {
        return std::begin(m_vecData);
    }
    Container<EmgData>::const_iterator end() const
    {
        return std::end(m_vecData);
    }
    Container<EmgData>::iterator end()
    {
        return std::end(m_vecData);
    }
    bool empty() const
    {
        return size() == 0;
    }
    Container<EmgData>::size_type size() const
    {
        return m_vecData.size();
    }
    void clear()
    {
        m_vecData.clear();
    }
    Container<EmgData>::const_iterator ByTimestamp(std::uint64_t const uiTimestamp) const
    {
        return std::find_if(std::begin(m_vecData), std::end(m_vecData),
        [uiTimestamp](EmgData const &emg)
        {
            return (emg.m_uiTimestamp == uiTimestamp);
        });
    }

signals:
    void EmgDataAdded();

private:
    Container<EmgData> m_vecData;
};

std::ostream &operator<<(std::ostream &os, EmgDataContainer const &emgData);
std::istream &operator>>(std::istream &os, EmgDataContainer &emgData);

} // namespace MyoVisualizer

#endif // ! MYOVIS_EMGDATACONTAINER_H

