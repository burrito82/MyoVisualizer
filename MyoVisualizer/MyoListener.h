#ifndef MYOVIS_MYOLISTENER_H
#define MYOVIS_MYOLISTENER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "EmgDataContainer.h"

#include <myo/myo.hpp>

#include <cstdint>
#include <functional>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
class MyoListener : public myo::DeviceListener
{
public:
    MyoListener();

    virtual void onEmgData(myo::Myo *myo, std::uint64_t timestamp, int8_t const *emg) override;
    virtual void onOrientationData(myo::Myo *myo, std::uint64_t timestamp, myo::Quaternion<float> const &rotation) override;
    virtual void onAccelerometerData(myo::Myo *myo, std::uint64_t timestamp, myo::Vector3<float> const &accel) override;
    virtual void onGyroscopeData(myo::Myo *myo, std::uint64_t timestamp, myo::Vector3<float> const &gyro) override;

    void SetOnOrientationData(std::function<void(myo::Quaternion<float> const &)> oFunction);
    void SetOnAccelerometerData(std::function<void(myo::Vector3<float> const &)> oFunction);
    void SetOnGyroscopeData(std::function<void(myo::Vector3<float> const &)> oFunction);

    myo::Quaternion<float> GetOrientation() const;
    myo::Vector3<float> GetAcceleration() const;
    myo::Vector3<float> GetGyroscope() const;

    EmgDataContainer const &GetEmgContainerRef() const;
    EmgDataContainer &GetEmgContainerRef();
    std::uint64_t GetLatestTimestamp() const;
private:
    EmgDataContainer m_oDataContainer;
    std::uint64_t m_uiLatestTimestamp;

    myo::Quaternion<float> m_qOrientation;
    myo::Vector3<float> m_v3Acceleration;
    myo::Vector3<float> m_v3Gyroscope;
    std::function<void(myo::Quaternion<float> const &)> m_funcOnOrientationData;
    std::function<void(myo::Vector3<float> const &)> m_funcOnAccelerometerData;
    std::function<void(myo::Vector3<float> const &)> m_funcOnGyroscopeData;
};

} // namespace MyoVisualizer

#endif // ! MYOVIS_MYOLISTENER_H

