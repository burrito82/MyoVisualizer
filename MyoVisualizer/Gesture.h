#ifndef MYOVIS_GESTURE_H
#define MYOVIS_GESTURE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <cstdint>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
struct Gesture
{
    Gesture();
public:
    using GestureId = char;
    Gesture(GestureId const &gestureId, std::uint64_t uiStartTime);
    static Gesture Construct(std::uint64_t uiStartTime,
        std::uint64_t uiEndTime, GestureId gestureId);

    void RecordEnd(std::uint64_t uiEndTime);

    std::uint64_t Start() const;
    std::uint64_t End() const;
    GestureId Id() const;

private:
    std::uint64_t m_uiStartTime;
    std::uint64_t m_uiEndTime;
    GestureId m_gestureId;
};

} // namespace MyoVisualizer

#endif // ! MYOVIS_GESTURE_H

