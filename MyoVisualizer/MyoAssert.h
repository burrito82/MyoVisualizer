#ifndef MYOVIS_ASSERT_H
#define MYOVIS_ASSERT_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <exception>
#include <stdexcept>
#include <string>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class MyoVisException : public std::runtime_error
{
public:
    explicit MyoVisException(char const *pErrorMsg):
        std::runtime_error{pErrorMsg}
    {

    }
    explicit MyoVisException(std::string const &strErrorMsg):
        std::runtime_error{strErrorMsg}
    {

    }
};

void Assert(bool bAssertion, char const *pErrorMsg = nullptr);
void Assert(bool bAssertion, std::string const &strErrorMsg);

} // namespace MyoVisualizer

#endif // ! MYOVIS_ASSERT_H

