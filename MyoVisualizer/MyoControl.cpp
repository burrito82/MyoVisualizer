#include "MyoControl.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "MyoListener.h"

#include <QMessageBox>

#include <thread>
#include <cmath>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
MyoControl::MyoControl(unsigned int uiIntervalMs):
    m_bListening{false},
    m_uiDurationMs{uiIntervalMs},
    m_pHub{nullptr},
    m_pMyo{nullptr},
    m_spListener{nullptr},
    m_oPollThread{}
{
    try
    {
        m_pHub = std::make_unique<myo::Hub>("de.root13.MyoVisualizer");
    }
    catch (...)
    {
        QMessageBox oErrorBox{};
        oErrorBox.setText("MyoHub initialization failed. Is MyoConnect running?");
        oErrorBox.setIcon(QMessageBox::Critical);
        oErrorBox.exec();
    }
}

MyoControl::~MyoControl()
{
    StopListening();
}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void MyoControl::Listen(std::shared_ptr<MyoListener> spListener)
{
    if (!m_bListening && spListener)
    {
        m_spListener = spListener;
        m_bListening = true;
        if (m_pHub && !m_pMyo)
        {
            m_pMyo = m_pHub->waitForMyo(1000u);
        }
        if (m_pMyo)
        {
            m_pMyo->setStreamEmg(myo::Myo::streamEmgEnabled);
            m_pHub->addListener(m_spListener.get());

            m_oPollThread = std::thread{std::bind(&MyoControl::Loop, this)};
        }
        else
        {
            QMessageBox oErrorBox{};
            oErrorBox.setText("Could not establish connection to myo device!");
            oErrorBox.setIcon(QMessageBox::Critical);
            oErrorBox.exec();

            m_oPollThread = std::thread{std::bind(&MyoControl::FakeLoop, this)};
        }
    }
}

void MyoControl::StopListening()
{
    if (m_bListening)
    {
        if (m_pHub)
        {
            m_pHub->removeListener(m_spListener.get());
        }
        m_bListening = false;
        m_oPollThread.join();
    }
}

void MyoControl::Loop()
{
    while (m_bListening && m_pHub)
    {
        m_pHub->run(m_uiDurationMs);
    }
}

void MyoControl::FakeLoop()
{
    uint64_t iTimeStamp = 0ull;
    int8_t emg[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    while (m_bListening)
    {
        for (int i = 0; i < 8; ++i)
        {
            float const ith = static_cast<float>(i + 1) / 8.0f;
            emg[i] = static_cast<int8_t>(std::sin(static_cast<float>(iTimeStamp * ith) / 10.0f) * (127.0f * ith));
        }
        m_spListener->onEmgData(nullptr, iTimeStamp, emg);
        iTimeStamp += 10000;
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

} // namespace MyoVisualizer
