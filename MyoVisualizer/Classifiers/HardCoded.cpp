#include "HardCoded.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../Gesture.h"

#include <QPoint>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
namespace Classifiers
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void HardCoded::Train(std::vector<EmgData> const &vecEmgs, std::vector<Gesture> const &vecGestures)
{
}

float HardCoded::Confidence() const
{
    return 0.0f;
}

std::vector<Gesture> HardCoded::Classify(std::vector<EmgData> const &vecEmgs)
{
    return{};
}

std::vector<Gesture> HardCoded::Classify(std::vector<QPointF> const &vecProcessed)
{
    std::vector<Gesture> gestures{};
    if (vecProcessed.empty())
    {
        return gestures;
    }
    std::uint64_t startTime{};
    bool bMidGesture = false;
    for (auto i = 1u; i < vecProcessed.size() - 1u; ++i)
    {
        auto x0 = vecProcessed[i - 1u].x();
        auto y0 = vecProcessed[i - 1u].y();
        auto y1 = vecProcessed[i].y();
        auto x2 = vecProcessed[i + 1u].x();
        auto y2 = vecProcessed[i + 1u].y();
        auto slope = (y2 - y0) / (x2 - x0);
        if (slope > 0.0005)
        {
            if (y2 > y1 && y1 > y0)
            {
                startTime = vecProcessed[i].x();
                bMidGesture = true;
            }
        }
        else
        {
            if (bMidGesture && slope < -0.0005)
            {
                if (y2 < y1 && y1 < y0)
                {
                    bMidGesture = false;
                    gestures.push_back(Gesture::Construct(startTime, vecProcessed[i].x(), '1'));
                }
            }
        }
    }
    if (bMidGesture)
    {
        gestures.push_back(Gesture::Construct(startTime, vecProcessed[vecProcessed.size() - 1u].x(), '1'));
    }
    return gestures;
}

} // namespace Classifiers
} // namespace MyoVisualizer
