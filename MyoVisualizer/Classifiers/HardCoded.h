#ifndef MYOVIS_CLASSIFIERS_HARDCODED_H
#define MYOVIS_CLASSIFIERS_HARDCODED_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../Classifier.h"
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
namespace MyoVisualizer
{
namespace Classifiers
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

class HardCoded : public IClassifier
{
public:
    virtual void Train(std::vector<EmgData> const &vecEmgs, std::vector<Gesture> const &vecGestures) override;
    virtual float Confidence() const override;
    virtual std::vector<Gesture> Classify(std::vector<EmgData> const &vecEmgs) override;
    virtual std::vector<Gesture> Classify(std::vector<QPointF> const &vecProcessed) override;
};

} // namespace Classifiers
} // namespace MyoVisualizer

#endif // ! MYOVIS_CLASSIFIERS_HARDCODED_H

