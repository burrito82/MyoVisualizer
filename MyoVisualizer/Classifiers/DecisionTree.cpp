#include "DecisionTree.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../Gesture.h"

#include <QPoint>

#include <iostream>
#include <fstream>
#include <memory>
#include <string>
#include <utility>
#include <vector>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
namespace Classifiers
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
namespace
{

static std::string const inf = "\xe2\x88\x9e";

struct InputData
{
    float timestamp, avg, diff, diff10, avgtm5, avgtp5, avgtm25, avgtp25, avgtm100, avgtp100;
};

class DecisionNode
{
public:
    enum Label
    {
        INACTIVE,
        ACTIVE
    };
    using InputType = std::vector<std::string>;
    DecisionNode(std::ifstream &is)
    {
        DecisionNode::InputType vecLines{};
        for (std::string strLine{}; std::getline(is, strLine);)
        {
            vecLines.push_back(std::move(strLine));
        }
        auto it = std::begin(vecLines);
        for (; it != std::end(vecLines) && (*it != ""); ++it)
        {
            children.push_back(std::make_unique<DecisionNode>(std::ref(it), std::end(vecLines), 0));
            std::cout << "\t=== Next ===" << std::endl;
        }
    }
    DecisionNode(InputType::const_iterator &it, InputType::const_iterator const &itEnd, int iDepth)
    {
        auto const strLine = *it;
        auto const itBegin = std::begin(strLine);
        auto const iDecisionParamBegin = 4u * iDepth;
        auto const iDecisionParamEnd = strLine.find_first_of(' ', iDecisionParamBegin);
        auto const strDecisionParam = std::string{itBegin + iDecisionParamBegin, itBegin + iDecisionParamEnd};
        decisionParam = GetOffset(strDecisionParam);
        auto const iRangeBegin = strLine.find_first_of('[') + 1u;
        auto const iRangeSeparatorBegin = strLine.find(" - ", iRangeBegin);
        auto const iRangeEnd = strLine.find_first_of(']', iRangeSeparatorBegin);
        range.first = stof(std::string{itBegin + iRangeBegin, itBegin + iRangeSeparatorBegin});
        range.second = stof(std::string{itBegin + iRangeSeparatorBegin + 3, itBegin + iRangeEnd});
        auto iLabelBegin = strLine.rfind(':');
        if (iLabelBegin != std::string::npos)
        {
            iLabelBegin += 2;
            auto iLabelEnd = strLine.find(' ', iLabelBegin);
            auto const strLabel = std::string{itBegin + iLabelBegin, itBegin + iLabelEnd};
            if (strLabel == "ACTIVE")
            {
                label = ACTIVE;
            }
        }
        for (auto i = 0; i < iDepth; ++i) std::cout << "|\t";
        std::cout << strDecisionParam << std::endl;
        ++it;
        for (; it != itEnd; ++it)
        {
            auto const curLine = *it;
            if (Depth(curLine) <= iDepth)
            {
                --it;
                break;
            }
            children.push_back(std::make_unique<DecisionNode>(it, itEnd, Depth(curLine)));
        }
    }

    bool Fits(InputData const &in) const
    {
        auto const val = in.*decisionParam;
        return (val >= range.first && val < range.second);
    }

    Label Eval(InputData const &in) const
    {
        if (children.empty())
        {
            return label;
        }
        else
        {
            for (auto const &child : children)
            {
                if (child->Fits(in))
                {
                    return child->Eval(in);
                }
            }
        }
    }

private:
    float InputData::*GetOffset(std::string const &str)
    {
        if (str == "avg") return &InputData::avg;
        if (str == "diff") return &InputData::diff;
        if (str == "diff10") return &InputData::diff10;
        if (str == "avgtm5") return &InputData::avgtm5;
        if (str == "avgtp5") return &InputData::avgtp5;
        if (str == "avgtm25") return &InputData::avgtm25;
        if (str == "avgtp25") return &InputData::avgtp25;
        if (str == "avgtm100") return &InputData::avgtm100;
        if (str == "avgtp100") return &InputData::avgtp100;
        return nullptr;
    }
    int Depth(std::string const &strLine)
    {
        int iDepth = 0;
        for (auto const c : strLine)
        {
            if (c == '|')
            {
                ++iDepth;
            }
        }
        return iDepth;
    }
    static float stof(std::string const &str)
    {
        if (str.find(inf) != std::string::npos)
        {
            if (str[0] == '-')
            {
                return -std::numeric_limits<float>::infinity();
            }
            else
            {
                return std::numeric_limits<float>::infinity();
            }
        }
        else
        {
            return std::stof(str);
        }
    }
    float InputData::*decisionParam;
    std::pair<float, float> range;
    Label label;

    std::vector<std::unique_ptr<DecisionNode>> children;
};

class DTClassifier
{
public:
    DTClassifier():
        decisionTree{std::ifstream{"./DecisionTrees/2015-08-17-1316.txt"}}
    {
    }
    std::vector<Gesture> Classify(std::vector<QPointF> const &data)
    {
        if (data.size() < 100)
            return{};
        std::vector<InputData> extendedData{};
        extendedData.reserve(data.size());
        for (auto const &dat : data)
        {
            InputData datx{};
            datx.timestamp = dat.x();
            datx.avg = dat.y();
            extendedData.push_back(std::move(datx));
        }
        for (auto i = 1u; i < extendedData.size() - 1u; ++i)
        {
            extendedData[i].diff = (extendedData[i + 1].avg - extendedData[i - 1].avg)
                / (extendedData[i + 1].timestamp - extendedData[i - 1].timestamp);
        }
        for (auto i = 10u; i < extendedData.size() - 10u; ++i)
        {
            extendedData[i].diff10 = (extendedData[i + 10].avg - extendedData[i - 10].avg)
                / (extendedData[i + 10].timestamp - extendedData[i - 10].timestamp);
        }
        for (auto i = 5u; i < extendedData.size(); ++i)
        {
            extendedData[i].diff = extendedData[i - 5].avg;
        }
        for (auto i = 0u; i < extendedData.size() - 5; ++i)
        {
            extendedData[i].diff = extendedData[i + 5].avg;
        }
        for (auto i = 25u; i < extendedData.size(); ++i)
        {
            extendedData[i].diff = extendedData[i - 25].avg;
        }
        for (auto i = 0u; i < extendedData.size() - 25; ++i)
        {
            extendedData[i].diff = extendedData[i + 25].avg;
        }
        for (auto i = 100u; i < extendedData.size(); ++i)
        {
            extendedData[i].diff = extendedData[i - 100u].avg;
        }
        for (auto i = 0u; i < extendedData.size() - 100u; ++i)
        {
            extendedData[i].diff = extendedData[i + 100u].avg;
        }

        std::vector<std::pair<float, DecisionNode::Label>> labels;
        for (auto const &in : extendedData)
        {
            labels.push_back(std::make_pair(in.timestamp, decisionTree.Eval(in)));
        }

        std::vector<Gesture> gestures{};
        auto it = std::begin(labels);
        std::uint64_t ullStartTime = 0;
        std::uint64_t ullEndTime = 0;
        bool bGestureActive = it->second == DecisionNode::ACTIVE;
        if (bGestureActive)
        {
            ullStartTime = it->first;
        }
        for (; it != std::end(labels); ++it)
        {
            if (bGestureActive && it->second == DecisionNode::ACTIVE)
            {
                ullEndTime = it->first;
                continue;
            }
            if (bGestureActive && it->second == DecisionNode::INACTIVE)
            {
                bGestureActive = false;
                gestures.push_back(Gesture::Construct(ullStartTime, ullEndTime, '1'));
            }
            if (!bGestureActive && it->second == DecisionNode::ACTIVE)
            {
                ullStartTime = it->first;
                bGestureActive = true;
            }
        }
        it = std::end(labels) - 1;
        if (it->second == DecisionNode::ACTIVE)
        {
            gestures.push_back(Gesture::Construct(ullStartTime, ullEndTime, '1'));
        }

        return gestures;
    }
private:
    DecisionNode decisionTree;
};

} // unnamed namespace
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
DecisionTree::DecisionTree()
{
}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void DecisionTree::Train(std::vector<EmgData> const &vecEmgs, std::vector<Gesture> const &vecGestures)
{
}

float DecisionTree::Confidence() const
{
    return 0.0f;
}

std::vector<Gesture> DecisionTree::Classify(std::vector<EmgData> const &vecEmgs)
{
    return{};
}

std::vector<Gesture> DecisionTree::Classify(std::vector<QPointF> const &vecProcessed)
{
    static DTClassifier S_oDTClassifier{};
    std::vector<Gesture> gestures = S_oDTClassifier.Classify(vecProcessed);
    if (vecProcessed.empty())
    {
        return gestures;
    }
    std::uint64_t startTime{};
    bool bMidGesture = false;
    for (auto i = 1u; i < vecProcessed.size() - 1u; ++i)
    {
        auto x0 = vecProcessed[i - 1u].x();
        auto y0 = vecProcessed[i - 1u].y();
        auto y1 = vecProcessed[i].y();
        auto x2 = vecProcessed[i + 1u].x();
        auto y2 = vecProcessed[i + 1u].y();
        auto slope = (y2 - y0) / (x2 - x0);
        if (slope > 0.05)
        {
            if (y2 > y1 && y1 > y0)
            {
                startTime = vecProcessed[i].x();
                bMidGesture = true;
            }
        }
        else
        {
            if (bMidGesture && slope < -0.05)
            {
                if (y2 < y1 && y1 < y0)
                {
                    bMidGesture = false;
                    gestures.push_back(Gesture::Construct(startTime, vecProcessed[i].x(), '1'));
                }
            }
        }
    }
    if (bMidGesture)
    {
        gestures.push_back(Gesture::Construct(startTime, vecProcessed[vecProcessed.size() - 1u].x(), '1'));
    }
    return gestures;
}

} // namespace Classifiers
} // namespace MyoVisualizer
