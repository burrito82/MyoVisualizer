#ifndef MYOVIS_SIGNALFILTERS_SMOOTH_H
#define MYOVIS_SIGNALFILTERS_SMOOTH_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../SignalFilter.h"

#include <algorithm>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
namespace SignalFilters
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
template<typename ContainerT>
class Smooth : public SignalFilter<ContainerT>
{
public:
    Smooth(std::shared_ptr<SignalFilter<ContainerT>> shFilteredSource, std::size_t iSmooth):
        SignalFilter{shFilteredSource},
        m_iSmooth{iSmooth}
    {

    }

    explicit Smooth(std::size_t iSmooth):
        Smooth{nullptr, iSmooth}
    {

    }

    using value_type = typename SignalFilter<ContainerT>::value_type;
private:
    virtual value_type Filter(ContainerT const &in, std::size_t iElement) override
    {
        int iBegin = std::max<int>(0, iElement - m_iSmooth);
        int iEnd = std::min<int>(in.size(), iElement + m_iSmooth);

        float fSmooth = 0.0f;
        for (int i = iBegin; i < iEnd; ++i)
        {
            fSmooth += in[i].y();
        }
        if (iBegin < iElement)
        {
            fSmooth /= static_cast<float>(iEnd - iBegin);
        }
        return QPointF{in[iElement].x(), fSmooth};

    }

    virtual std::size_t ValidBeginImpl() const override
    {
        return m_iSmooth;
    }

    std::size_t m_iSmooth;
};

} // namespace SignalFilters
} // namespace MyoVisualizer

#endif // ! MYOVIS_SIGNALFILTERS_SMOOTH_H

