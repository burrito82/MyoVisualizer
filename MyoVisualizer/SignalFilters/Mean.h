#ifndef MYOVIS_SIGNALFILTERS_MEAN_H
#define MYOVIS_SIGNALFILTERS_MEAN_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../SignalFilter.h"

#include <algorithm>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
namespace SignalFilters
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
template<typename ContainerT>
class Mean : public SignalFilter<ContainerT>
{
public:
    Mean(std::shared_ptr<SignalFilter<ContainerT>> shFilteredSource, std::size_t iWindowSize):
        SignalFilter{shFilteredSource},
        m_iWindowSize{iWindowSize}
    {

    }

    explicit Mean(std::size_t iWindowSize):
        Mean{nullptr, iWindowSize}
    {

    }

    using value_type = typename SignalFilter<ContainerT>::value_type;
private:
    virtual value_type Filter(ContainerT const &in, size_t iElement) override
    {
        int iBegin = std::max<int>(0, iElement - m_iWindowSize);
        float fMean = 0.0f;
        if (iBegin < iElement)
        {
            for (int i = iBegin; i < iElement; ++i)
            {
                fMean += in[i].y();
            }
            fMean /= static_cast<float>(iElement - iBegin);
        }
        return QPointF{in[iElement].x(), fMean};

    }

    virtual std::size_t ValidBeginImpl() const override
    {
        return m_iWindowSize;
    }

    std::size_t m_iWindowSize;
};

} // namespace SignalFilters
} // namespace MyoVisualizer

#endif // ! MYOVIS_SIGNALFILTERS_MEAN_H

