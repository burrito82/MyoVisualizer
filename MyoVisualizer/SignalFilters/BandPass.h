#ifndef MYOVIS_SIGNALFILTERS_BANDPASS_H
#define MYOVIS_SIGNALFILTERS_BANDPASS_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../SignalFilter.h"
#include "../extInclude/filt.h"

#include <algorithm>
#include <array>
#include <iterator>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
namespace SignalFilters
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
template<typename ContainerT>
class BandPass : public SignalFilter<ContainerT>
{
public:
    explicit BandPass(std::shared_ptr<SignalFilter<ContainerT>> shFilteredSource):
        SignalFilter{shFilteredSource},
        m_aFilters{},
        m_dSampleRate{0.1},
        m_iTaps{205},
        m_aCache{}
    {

    }

    BandPass():
        BandPass{nullptr}
    {

    }

    void SetLowPass(double dLow)
    {
        dLow = std::min(std::max(dLow, 0.000001) / 1000.0, m_dSampleRate / 2.000001);
        for (auto &pFilter : m_aFilters)
        {
            pFilter = std::make_shared<ext::Filter>(ext::LPF, m_iTaps, m_dSampleRate, dLow);
        }
        ClearCache();
    }

    void SetHighPass(double dHigh)
    {
        dHigh = std::min(dHigh / 1000.0, m_dSampleRate / 2.000001);
        for (auto &pFilter : m_aFilters)
        {
            pFilter = std::make_shared<ext::Filter>(ext::HPF, m_iTaps, m_dSampleRate, dHigh);
        }
        ClearCache();
    }

    void SetBandPass(double dLow, double dHigh)
    {
        if (dLow <= 0.0)
        {
            SetLowPass(dHigh);
            return;
        }
        dLow = std::min(dLow / 1000.0, m_dSampleRate / 2.000001);
        dHigh = std::max(dLow * 1.00001, std::min(dHigh / 1000.0, m_dSampleRate / 2.000001));
        for (auto &pFilter : m_aFilters)
        {
            pFilter = std::make_shared<ext::Filter>(ext::BPF, m_iTaps, m_dSampleRate, dLow, dHigh);
        }
        ClearCache();
    }

    void SetPassthrough()
    {
        for (auto &pFilter : m_aFilters)
        {
            pFilter = nullptr;
        }
        ClearCache();
    }

    using value_type = typename SignalFilter<ContainerT>::value_type;

private:
    void ClearCache()
    {
        for (auto &liCache : m_aCache)
        {
            liCache.clear();
        }
    }

    virtual value_type Filter(ContainerT const &in, std::size_t iElement, int iSensorId) override
    {
        static std::mutex S_oMutex{};

        auto spFilter = m_aFilters[iSensorId];
        if (spFilter != nullptr && S_oMutex.try_lock())
        {
            std::lock_guard<std::mutex> oLock{S_oMutex, std::adopt_lock};
            auto &cache = m_aCache[iSensorId];
            if (iElement == 0)
            {
                for (auto it = std::begin(cache);
                     (it != std::end(cache)) && (it->x() < in[0].x());
                     it = cache.erase(it))
                {
                    // erase old data
                }

                for (auto it = std::begin(in) + std::min(cache.size(), in.size());
                     it != std::end(in);
                     ++it)
                {
                    cache.push_back(QPointF{it->x(), std::abs(spFilter->do_sample(it->y()))});
                    //cache.push_back(QPointF{it->x(), spFilter->do_sample(it->y())});
                    //cache.push_back(QPointF{it->x(), std::min(std::max(spFilter->do_sample(it->y()), 0.0), 128.0)});
                }
            }

            if (iElement >= cache.size())
            {
                std::cout << "error, tried to access more data than filtered" << std::endl;
                return QPointF{in[iElement].x(), in[iElement].y()};
            }
            auto it = std::begin(cache);
            std::advance(it, iElement);
            return *it;
            //return QPointF{in[iElement].x(), m_aCache[iSensorId][iElement]};
        }
        return QPointF{in[iElement].x(), in[iElement].y()};
    }

    virtual value_type Filter(ContainerT const &in, std::size_t iElement) override
    {
        // Should not be called!
        throw std::exception{};
    }

    virtual std::size_t ValidBeginImpl() const override
    {
        return 0;
    }

    std::array<std::shared_ptr<ext::Filter>, 8> m_aFilters;
    double m_dSampleRate;
    int m_iTaps;
    std::array<std::list<QPointF>, 8> m_aCache;
};

} // namespace SignalFilters
} // namespace MyoVisualizer

#endif // ! MYOVIS_SIGNALFILTERS_BANDPASS_H

