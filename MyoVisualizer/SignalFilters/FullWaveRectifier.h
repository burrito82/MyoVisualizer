#ifndef MYOVIS_SIGNALFILTERS_FULLWAVERECTIFIER_H
#define MYOVIS_SIGNALFILTERS_FULLWAVERECTIFIER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../SignalFilter.h"

#include <algorithm>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
namespace SignalFilters
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
template<typename ContainerT>
class FullWaveRectifier : public SignalFilter<ContainerT>
{
public:
    using value_type = typename SignalFilter<ContainerT>::value_type;
private:
    virtual value_type Filter(ContainerT const &in, size_t iElement) override
    {
        return QPointF{in[iElement].x(), std::abs(in[iElement].y())};
    }
};

} // namespace SignalFilters
} // namespace MyoVisualizer

#endif // ! MYOVIS_SIGNALFILTERS_FULLWAVERECTIFIER_H

