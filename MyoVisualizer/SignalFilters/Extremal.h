#ifndef MYOVIS_SIGNALFILTERS_EXTREMAL_H
#define MYOVIS_SIGNALFILTERS_EXTREMAL_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "../SignalFilter.h"

#include <algorithm>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
namespace SignalFilters
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
template<typename ContainerT>
class Extremal : public SignalFilter<ContainerT>
{
public:
    enum ExtremalType
    {
        MIN,
        MAX
    };

    Extremal(std::shared_ptr<SignalFilter<ContainerT>> shFilteredSource, ExtremalType eType):
        SignalFilter{shFilteredSource},
        m_eType{eType},
        m_fMaximalMinimum{150.0f},
        m_fMinimalMaximum{-150.0f}
    {

    }

    using value_type = typename SignalFilter<ContainerT>::value_type;
private:
    virtual value_type Filter(ContainerT const &in, std::size_t iElement) override
    {
        float fYValue = 200.0f;
        int iRange = 15;
        if (iElement > iRange && iElement + iRange < in.size())
        {
            if (m_eType == MIN)
            {
                fYValue = -fYValue;
                if (in[iElement].y() < m_fMaximalMinimum)
                {
                    bool bIsMinimum = true;
                    for (int i = iElement - iRange; i < iElement + iRange && bIsMinimum; ++i)
                    {
                        if ((in[i].y() < in[iElement].y())
                            || (in[i].y() < in[iElement].y()))
                        {
                            bIsMinimum = false;
                            break;
                        }
                    }
                    if (bIsMinimum)
                    {
                        fYValue = in[iElement].y();
                    }
                }
            }
            else
            {
                if (in[iElement].y() > m_fMinimalMaximum)
                {
                    bool bIsMaximum = true;
                    for (int i = iElement - iRange; i < iElement + iRange && bIsMaximum; ++i)
                    {
                        if ((in[i].y() > in[iElement].y())
                            || (in[i].y() > in[iElement].y()))
                        {
                            bIsMaximum = false;
                            break;
                        }
                    }
                    if (bIsMaximum)
                    {
                        fYValue = in[iElement].y();
                    }
                }
            }
        }
        return QPointF{in[iElement].x(), fYValue};

    }

    ExtremalType const m_eType;
    float const m_fMaximalMinimum;
    float const m_fMinimalMaximum;
};

} // namespace SignalFilters
} // namespace MyoVisualizer

#endif // ! MYOVIS_SIGNALFILTERS_EXTREMAL_H

