#ifndef MYOVIS_GYROSCOPE_H
#define MYOVIS_GYROSCOPE_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <cstdint>
#include <functional>
#include <iosfwd>
#include <tuple>
#include <utility>
#include <vector>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
class Gyroscope
{
public:
    enum GyroState
    {
        ARM_UNSTABLE,
        ARM_STABILIZING,
        ARM_STABLE
    };
    using GyroData = std::tuple<float, std::uint64_t>;

    Gyroscope();

    void Log(float fGyroMag, std::uint64_t uiTimestamp);

    std::uint64_t GetStableBegin() const;
    std::uint64_t GetUnstableBegin() const;
    GyroState GetState() const;

    void SetOnArmStable(std::function<void(float fValue, std::uint64_t uiTimestamp)> funcOnArmStable);
    void SetOnArmStabilizing(std::function<void(float fValue, std::uint64_t uiTimestamp)> funcOnArmStabilizing);
    void SetOnArmUnstable(std::function<void(float fValue, std::uint64_t uiTimestamp)> funcOnArmUnstable);

    void Export(std::ostream &os) const;
private:
    void LogToHistory(std::uint64_t uiTimestamp);

    std::uint64_t m_uiStableBegin;
    std::uint64_t m_uiStabilizingBegin;
    std::uint64_t m_uiUnstableBegin;
    GyroState m_eCurrentState;
    std::function<void(float fValue, std::uint64_t uiTimestamp)> m_funcOnArmStable;
    std::function<void(float fValue, std::uint64_t uiTimestamp)> m_funcOnArmStabilizing;
    std::function<void(float fValue, std::uint64_t uiTimestamp)> m_funcOnArmUnstable;
    std::vector<std::pair<std::uint64_t, GyroState>> m_vecHistory;
};

std::ostream &operator<<(std::ostream &os, Gyroscope const &gyro);

} // namespace MyoVisualizer

#endif // ! MYOVIS_GYROSCOPE_H

