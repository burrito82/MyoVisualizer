#include "Gyroscope.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <ostream>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
Gyroscope::Gyroscope():
    m_uiStableBegin{},
    m_uiStabilizingBegin{},
    m_uiUnstableBegin{},
    m_eCurrentState{},
    m_funcOnArmStable{},
    m_funcOnArmStabilizing{},
    m_funcOnArmUnstable{},
    m_vecHistory{}
{

}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void Gyroscope::Log(float fGyroMag, std::uint64_t uiTimestamp)
{
    float const fThreshold = 10.0f;
    if ((m_eCurrentState != ARM_UNSTABLE)
        && fGyroMag > fThreshold)
    {
        m_eCurrentState = ARM_UNSTABLE;
        m_uiUnstableBegin = uiTimestamp;
        if (static_cast<bool>(m_funcOnArmUnstable))
        {
            m_funcOnArmUnstable(fGyroMag, uiTimestamp);
        }
        LogToHistory(uiTimestamp);
        return;
    }
    if (m_eCurrentState == ARM_UNSTABLE
        && fGyroMag < fThreshold
        && (uiTimestamp - m_uiUnstableBegin) > 1000000)
    {
        m_eCurrentState = ARM_STABILIZING;
        m_uiStabilizingBegin = uiTimestamp;
        if (static_cast<bool>(m_funcOnArmStabilizing))
        {
            m_funcOnArmStabilizing(fGyroMag, uiTimestamp);
        }
        LogToHistory(uiTimestamp);
        return;
    }
    if (m_eCurrentState == ARM_STABILIZING
        && fGyroMag < fThreshold
        && (uiTimestamp - m_uiStabilizingBegin) > 2000000)
    {
        m_eCurrentState = ARM_STABLE;
        m_uiStableBegin = uiTimestamp;
        if (static_cast<bool>(m_funcOnArmStable))
        {
            m_funcOnArmStable(fGyroMag, uiTimestamp);
        }
        LogToHistory(uiTimestamp);
        return;
    }
}

std::uint64_t Gyroscope::GetStableBegin() const
{
    return m_uiStableBegin;
}

std::uint64_t Gyroscope::GetUnstableBegin() const
{
    return m_uiUnstableBegin;
}

Gyroscope::GyroState Gyroscope::GetState() const
{
    return m_eCurrentState;
}

void Gyroscope::SetOnArmStable(std::function<void(float fValue, std::uint64_t uiTimestamp)> funcOnArmStable)
{
    m_funcOnArmStable = std::move(funcOnArmStable);
}

void Gyroscope::SetOnArmStabilizing(std::function<void(float fValue, std::uint64_t uiTimestamp)> funcOnArmStabilizing)
{
    m_funcOnArmStabilizing = std::move(funcOnArmStabilizing);
}

void Gyroscope::SetOnArmUnstable(std::function<void(float fValue, std::uint64_t uiTimestamp)> funcOnArmUnstable)
{
    m_funcOnArmUnstable = std::move(funcOnArmUnstable);
}

void Gyroscope::LogToHistory(std::uint64_t uiTimestamp)
{
    m_vecHistory.push_back(std::make_pair(uiTimestamp, m_eCurrentState));
}

void Gyroscope::Export(std::ostream &os) const
{
    for (auto const &log : m_vecHistory)
    {
        os << log.first << ";" << static_cast<int>(log.second) << "\n";
    }
}

std::ostream &operator<<(std::ostream &os, Gyroscope const &gyro)
{
    gyro.Export(os);
    return os;
}

} // namespace MyoVisualizer
