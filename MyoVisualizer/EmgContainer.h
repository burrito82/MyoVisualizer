#ifndef MYOVIS_EMGCONTAINER_H
#define MYOVIS_EMGCONTAINER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "MyoAssert.h"

#include <iterator>
#include <vector>

#include <cstddef>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
template<typename T, size_t> class EmgContainer;
template<typename T, size_t> class EmgContainerIterator;
template<typename T, size_t> class EmgContainerNode;
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

template<typename T, size_t iNodeLength>
class EmgContainerNode
{
public:
    using value_type = T;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using reference = T &;
    using const_reference = T const &;
    using pointer = T *;
    using const_pointer = T const *;
    using iterator = pointer;
    using const_iterator = const_pointer;

    EmgContainerNode():
        m_aRaw{},
        m_pNext{nullptr},
        end_{AsT()},
        m_iSize(0)
    {

    }
    ~EmgContainerNode()
    {
        delete m_pNext;
        for (pointer ptr = AsT(); ptr != end_; ++ptr)
        {
            ptr->~T();
        }
    }

    EmgContainerNode<T, iNodeLength> &operator=(EmgContainerNode<T, iNodeLength> const &source)
    {
        delete m_pNext;
        m_pNext = nullptr;
        for (pointer ptr = AsT(); ptr != end_; ++ptr)
        {
            ptr->~T();
        }
        if (source.m_pNext != nullptr)
        {
            m_pNext = new EmgContainerNode<T, iNodeLength>{};
            *m_pNext = *source.m_pNext;
        }
        m_iSize = 0;
        end_ = AsT();
        for (auto src = std::begin(source); src != std::end(source); ++src, ++end_, ++m_iSize)
        {
            new (end_) T{*src};
        }
        Assert(m_iSize == source.m_iSize);
        return *this;
    }

    reference operator[](size_t index)
    {
        Assert(index < m_iSize, "EmgContainerNode::operator[] out of bounds");
        return AsT()[index];
    }
    const_reference operator[](size_t index) const
    {
        Assert(index < m_iSize, "EmgContainerNode::operator[] out of bounds");
        return AsT()[index];
    }
    iterator begin()
    {
        return AsT();
    }
    const_iterator begin() const
    {
        return AsT();
    }
    iterator end()
    {
        return end_;
    }
    const_iterator end() const
    {
        return end_;
    }
    bool empty() const
    {
        return end_ == AsT();
    }
    size_type size() const
    {
        return m_iSize;
    }
    size_type capacity() const
    {
        return iNodeLength;
    }
    void push_back(T const &value)
    {
        if (m_iSize < iNodeLength)
        {
            new (end_++) T{value};
            ++m_iSize;
        }
        else
        {
            if (m_pNext == nullptr)
            {
                m_pNext = new EmgContainerNode<T, iNodeLength>{};
            }
            m_pNext->push_back(value);
        }
    }

    EmgContainerNode *Next()
    {
        return m_pNext;
    }

    EmgContainerNode const *Next() const
    {
        return m_pNext;
    }

private:
    T *AsT()
    {
        return static_cast<T *>(static_cast<void *>(&m_aRaw[0]));
    }
    T const *AsT() const
    {
        return static_cast<T const *>(static_cast<void const *>(&m_aRaw[0]));
    }
    unsigned char m_aRaw[sizeof(T[iNodeLength])];
    EmgContainerNode *m_pNext;
    pointer end_;
    size_type m_iSize;
};

template<typename T, size_t iNodeLength>
class EmgContainerIterator
{
public:
    using value_type = T;
    using difference_type = std::ptrdiff_t;
    using pointer = T *;
    using reference = T &;
    using iterator_category = typename std::random_access_iterator_tag;

    // iterator
    explicit EmgContainerIterator(EmgContainer<T, iNodeLength> *pContainer = nullptr, 
                                  std::size_t iNode = 0):
        m_pContainer{pContainer},
        m_iCurrent(iNode)
    {

    }

    reference operator*()
    {
        Assert(m_pContainer != nullptr, "EmgContainerIterator::operator*(nullptr)");
        return (*m_pContainer)[m_iCurrent];
    }

    pointer operator->()
    {
        return &this->operator*();
    }

    EmgContainerIterator<T, iNodeLength> &operator++()
    {
        ++m_iCurrent;
        return *this;
    }

    // InputIterator, ForwardIterator
    EmgContainerIterator<T, iNodeLength> operator++(int)
    {
        auto oCopy = *this;
        ++m_iCurrent;
        return oCopy;
    }
    bool operator==(EmgContainerIterator<T, iNodeLength> const &other) const
    {
        return (m_pContainer == other.m_pContainer
                && m_iCurrent == other.m_iCurrent);
    }
    bool operator!=(EmgContainerIterator<T, iNodeLength> const &other) const
    {
        return !(*this == other);
    }

    // BidirectionalIterator
    EmgContainerIterator<T, iNodeLength> &operator--()
    {
        --m_iCurrent;
        return *this;
    }
    EmgContainerIterator<T, iNodeLength> operator--(int)
    {
        auto oCopy = *this;
        --m_iCurrent;
        return oCopy;
    }

    // RandomAccessIterator
    EmgContainerIterator<T, iNodeLength> &operator+=(difference_type iDiff)
    {
        m_iCurrent += iDiff;
        return *this;
    }
    EmgContainerIterator<T, iNodeLength> operator+(difference_type iDiff) const
    {
        auto oCopy = *this;
        oCopy.m_iCurrent += iDiff;
        return oCopy;
    }
    EmgContainerIterator<T, iNodeLength> &operator-=(difference_type iDiff)
    {
        m_iCurrent -= iDiff;
        return *this;
    }
    EmgContainerIterator<T, iNodeLength> operator-(difference_type iDiff) const
    {
        auto oCopy = *this;
        oCopy.m_iCurrent -= iDiff;
        return oCopy;
    }
    difference_type operator-(EmgContainerIterator<T, iNodeLength> const &rhs) const
    {
        return m_iCurrent - rhs.m_iCurrent;
    }
    bool operator<(EmgContainerIterator<T, iNodeLength> const &rhs) const
    {
        return m_iCurrent < rhs.m_iCurrent;
    }
    bool operator>(EmgContainerIterator<T, iNodeLength> const &rhs) const
    {
        return m_iCurrent > rhs.m_iCurrent;
    }
    bool operator>=(EmgContainerIterator<T, iNodeLength> const &rhs) const
    {
        return m_iCurrent >= rhs.m_iCurrent;
    }
    bool operator<=(EmgContainerIterator<T, iNodeLength> const &rhs) const
    {
        return m_iCurrent <= rhs.m_iCurrent;
    }
private:
    EmgContainer<T, iNodeLength> *m_pContainer;
    std::size_t m_iCurrent;
};

template<typename T, size_t iNodeLength>
EmgContainerIterator<T, iNodeLength> operator+(typename EmgContainerIterator<T, iNodeLength>::difference_type iDiff, EmgContainerIterator<T, iNodeLength> const &it)
{
    return (it + iDiff);
}

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
template<typename T, size_t iNodeLength = 0x400>
class EmgContainer
{
public:
    using value_type = T;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using reference = T &;
    using const_reference = T const &;
    using pointer = T *;
    using const_pointer = T const *;
    using iterator = EmgContainerIterator<T, iNodeLength>;
    using const_iterator = EmgContainerIterator<T, iNodeLength>;

    EmgContainer():
        m_oHead{},
        m_pLastNode{&m_oHead},
        m_vecAllNodes{}
    {
        m_vecAllNodes.push_back(m_pLastNode);
    }

    reference operator[](size_t index)
    {
        size_t iNode = index / iNodeLength;
        size_t iElem = index % iNodeLength;
        return (*m_vecAllNodes[iNode])[iElem];
    }
    const_reference operator[](size_t index) const
    {
        size_t iNode = index / iNodeLength;
        size_t iElem = index % iNodeLength;
        return (*m_vecAllNodes[iNode])[iElem];
    }
    reference back()
    {
        return *(begin() + size() - 1);
    }
    const_reference back() const
    {
        return *(begin() + size() - 1);
    }
    iterator begin()
    {
        return EmgContainerIterator<T, iNodeLength>{this, 0};
    }
    const_iterator begin() const
    {   
        return EmgContainerIterator<T, iNodeLength>{const_cast<EmgContainer<T, iNodeLength> *>(this), static_cast<std::size_t>(0)};
    }
    iterator end()
    {
        return EmgContainerIterator<T, iNodeLength>{this, size()};
    }
    const_iterator end() const
    {
        return EmgContainerIterator<T, iNodeLength>{const_cast<EmgContainer<T, iNodeLength> *>(this), size()};
    }
    bool empty() const
    {
        return size() == 0;
    }
    size_type size() const
    {
        return iNodeLength * std::max<int>(0, m_vecAllNodes.size() - 1) + m_pLastNode->size();
    }
    void reserve(size_type)
    {

    }
    size_type capacity()
    {
        return iNodeLength * std::max<int>(0, m_vecAllNodes.size() - 1) + m_pLastNode->capacity();
    }
    void clear()
    {
        m_vecAllNodes.clear();
        m_oHead = EmgContainerNode<T, iNodeLength>{};
        m_pLastNode = &m_oHead;
        m_vecAllNodes.push_back(m_pLastNode);
    }
    void push_back(T const &value)
    {
        m_pLastNode->push_back(value);
        if (m_pLastNode->Next() != nullptr)
        {
            m_pLastNode = m_pLastNode->Next();
            m_vecAllNodes.push_back(m_pLastNode);
        }
    }

private:
    EmgContainerNode<T, iNodeLength> m_oHead;
    EmgContainerNode<T, iNodeLength> *m_pLastNode;
    std::vector<EmgContainerNode<T, iNodeLength> *> m_vecAllNodes;
};

} // namespace MyoVisualizer

#endif // ! MYOVIS_EMGCONTAINER_H

