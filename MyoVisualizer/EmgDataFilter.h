#ifndef MYOVIS_EMGDATAFILTER_H
#define MYOVIS_EMGDATAFILTER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "EmgDataContainer.h"
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
class EmgDataFilter
{
public:
    template<typename T> using Container = EmgDataContainer::Container<T>;

    EmgDataFilter(EmgDataContainer const &rData);
    virtual ~EmgDataFilter() = default;

    void Update();

    Container<EmgData>::size_type From() const;
    Container<EmgData>::size_type To() const;

    Container<EmgData>::const_reference operator[](size_t index) const;
    Container<EmgData>::const_iterator begin() const;
    Container<EmgData>::const_iterator end() const;
    Container<EmgData>::size_type size() const;
    bool empty() const;

protected:
    virtual void UpdateImpl() = 0;
    virtual Container<EmgData>::const_iterator beginImpl() const = 0;
    virtual Container<EmgData>::const_iterator endImpl() const = 0;

    EmgDataContainer const &m_rDataContainer;
private:
};

} // namespace MyoVisualizer

#endif // ! MYOVIS_EMGDATAFILTER_H

