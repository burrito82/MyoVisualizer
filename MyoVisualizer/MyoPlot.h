#ifndef MYOVIS_MYOPLOT_H
#define MYOVIS_MYOPLOT_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "qwt_plot.h"

#include <qpoint.h>

#include <array>
#include <memory>
#include <vector>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class QwtPlotCurve;
namespace MyoVisualizer
{
class EmgDataFilter;
template<typename ContainerT> class SignalFilter;
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/
/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
class MyoPlot : public QwtPlot
{
    Q_OBJECT
public:
    MyoPlot(QWidget *parent = nullptr);

    void StartTimer(int iIntervalMs = 20);
    void StopTimer();

    void SetId(size_t iIndex);
    void SetFilter(std::shared_ptr<EmgDataFilter> pFilter);
    void SetPlotCurves(std::array<QwtPlotCurve *, 8> aPlotCurves);

    void AddSignalFilter(std::shared_ptr<SignalFilter<std::vector<QPointF>>> spSignalFilter);
    std::vector<std::shared_ptr<SignalFilter<std::vector<QPointF>>>> &GetSignalFilters();

protected:
    virtual void timerEvent(QTimerEvent *pEvent) override;

private:
    void Update();

    int m_iTimerId;
    int m_iId;
    std::shared_ptr<EmgDataFilter> m_spDataFilter;
    std::array<QwtPlotCurve *, 8> m_aPlotCurves;
    std::vector<std::shared_ptr<SignalFilter<std::vector<QPointF>>>> m_vecSignalFilters;
};

} // namespace MyoVisualizer

#endif // ! MYOVIS_MYOPLOTVIEW_H

