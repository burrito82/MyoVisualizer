#ifndef MYOVIS_GESTURERECORDER_H
#define MYOVIS_GESTURERECORDER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "Gesture.h"

#include <QObject>

#include <cstdint>
#include <deque>
#include <iosfwd>
#include <map>
#include <string>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
class GestureRecorder : public QObject
{
    Q_OBJECT
public:
    using GestureId = Gesture::GestureId;
    using Container_Gesture = std::deque<Gesture>;
    using const_iterator = Container_Gesture::const_iterator;

    void SetGestureName(GestureId gestureId, std::string const &strName);
    std::map<GestureId, std::string> const &GetGestureNames() const;
    void Import(std::istream &is);
    void Export(std::ostream &os, std::string const &strGestureName) const;
    void Export(std::ostream &os, GestureId gestureId) const;

    bool GestureActive(GestureId gestureId) const;
    Container_Gesture const &GetCompletedGestures() const;
    Gesture const *GetActiveGesture(GestureId id) const;

    const_iterator begin() const;
    const_iterator end() const;
    void clear();

public slots:
    void RecordGestureStart(GestureId const &gestureId);
    void RecordGestureEnd(GestureId const &gestureId);
    void SetCurrentTimestamp(std::uint64_t uiCurrentTimestamp);

private:
    std::uint64_t m_uiCurrentTimestamp;
    std::map<GestureId, std::string> m_mapGestureNames;
    std::map<GestureId, Gesture> m_mapUnfinishedGestures;
    Container_Gesture m_deqCompleteGestures;
};

std::ostream &operator<<(std::ostream &os, GestureRecorder::Container_Gesture &container);
std::istream &operator>>(std::istream &is, GestureRecorder::Container_Gesture &container);

} // namespace MyoVisualizer

#endif // ! MYOVIS_GESTURERECORDER_H

