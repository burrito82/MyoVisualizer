#ifndef MYOVIS_MYOCONTROL_H
#define MYOVIS_MYOCONTROL_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "Assert.h"

#include <myo/myo.hpp>

#include <functional>
#include <memory>
#include <thread>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
class MyoListener;
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * Wraps the MyoHub.
 */
class MyoControl
{
public:
    explicit MyoControl(unsigned int uiIntervalMs = 10u);
    ~MyoControl();
    
    void Listen(std::shared_ptr<MyoListener> spListener);
    void StopListening();

    void Loop();
    void FakeLoop();
private:
    bool m_bListening;
    unsigned int m_uiDurationMs;
    std::unique_ptr<myo::Hub> m_pHub;
    myo::Myo *m_pMyo;
    std::shared_ptr<MyoListener> m_spListener;
    std::thread m_oPollThread;
};

} // namespace MyoVisualizer

#endif // ! MYOVIS_MYOCONTROL_H

