#include "EmgDataFilterSlidingWindow.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <algorithm>
#include <iterator>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

EmgDataFilterSlidingWindow::EmgDataFilterSlidingWindow(EmgDataContainer const &rData):
    EmgDataFilter{rData},
    m_iWindowSize{1000},
    begin_{std::begin(rData)},
    end_{std::begin(rData)}
{

}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void EmgDataFilterSlidingWindow::SetBegin(size_t iBegin)
{
    m_iBegin = iBegin;
    Update();
}

size_t EmgDataFilterSlidingWindow::GetBegin() const
{
    return m_iBegin;
}

void EmgDataFilterSlidingWindow::SetWindowSize(size_t iWindowSize)
{
    m_iWindowSize = iWindowSize;
    Update();
}

size_t EmgDataFilterSlidingWindow::GetWindowSize() const
{
    return m_iWindowSize;
}

EmgDataFilterSlidingWindow::Container<EmgData>::const_iterator EmgDataFilterSlidingWindow::beginImpl() const
{
    return begin_;
}

EmgDataFilterSlidingWindow::Container<EmgData>::const_iterator EmgDataFilterSlidingWindow::endImpl() const
{
    return end_;
}

void EmgDataFilterSlidingWindow::UpdateImpl()
{
    using diff_t = Container<EmgData>::difference_type;
    auto iEffectiveBegin = std::min<diff_t>(m_iBegin, m_rDataContainer.size());

    auto const realEnd = std::end(m_rDataContainer);
    begin_ = std::begin(m_rDataContainer) + iEffectiveBegin;
    end_ = begin_;
    while (end_ != realEnd
           && (end_->m_uiTimestamp - begin_->m_uiTimestamp < m_iWindowSize * 1000))
    {
        ++end_;
    }
}

} // namespace MyoVisualizer
