#include "MyoPlot.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "EmgDataFilter.h"
#include "SignalFilter.h"

#include <qwt_plot.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <qwt_series_data.h>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
namespace
{

static QPointF EmgToPoint(EmgData const &emg, int iCounter, size_t iSensor)
{
    return QPointF
    {
        //static_cast<float>(iCounter),
        static_cast<float>(emg.m_uiTimestamp) / 1000.0f, // convert to milliseconds
        static_cast<float>(emg[iSensor])
    };
}

template<typename ContainerT>
static ContainerT Extract(EmgDataFilter *pFilter, std::size_t iSensor)
{
    using namespace std;
    ContainerT vecResult{};
    vecResult.reserve(pFilter->size());
    int iCounter = 0;
    for (auto emg : *pFilter)
    {
        vecResult.push_back(EmgToPoint(emg, iCounter++, iSensor));
    }
    sort(begin(vecResult), end(vecResult),
              [](QPointF const &lhs, QPointF const &rhs) -> bool
    {
        return lhs.x() < rhs.x();
    });
    auto const eraseBegin = std::unique(begin(vecResult), end(vecResult),
                                  [](QPointF const &lhs, QPointF const &rhs) -> bool
    {
        return lhs.x() == rhs.x();
    });
    vecResult.erase(eraseBegin, end(vecResult));
    return vecResult;
}

template<typename ContainerT>
class PlotSeries : public QwtSeriesData<typename ContainerT::value_type>
{
public:
    using value_type = typename ContainerT::value_type;
    PlotSeries():
        m_vecData()
    {
    }
    PlotSeries(ContainerT const &cData):
        m_vecData(cData)
    {
    }

    template<typename IteratorT>
    void SetData(IteratorT const &begin, IteratorT const &end)
    {
        for (auto it = begin; it != end; ++it)
        {
            m_vecData.push_back(*it);
        }
    }

    template<typename AnotherContainerT>
    void SetData(AnotherContainerT const &cData)
    {
        for (auto p : cData)
        {
            m_vecData.push_back(p);
        }
    }
    virtual std::size_t size() const override
    {
        return m_vecData.size();
    }
    value_type sample(std::size_t i) const override
    {
        return m_vecData[i];
    }
    virtual QRectF boundingRect() const override
    {
        auto iLeft = static_cast<float>(std::begin(m_vecData)->x());
        auto iWidth = static_cast<float>((std::end(m_vecData) - 1)->x()) - iLeft;
        //return QRectF{0.0, -128.0, static_cast<double>(size()), 256.0};
        //return QRectF{0.0, 0.0, static_cast<double>(size()), 128.0};
        return QRectF(iLeft, 0.0, iWidth, 128.0);
    }
private:
    ContainerT m_vecData;
};

}
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
MyoPlot::MyoPlot(QWidget *parent):
    QwtPlot{parent},
    m_iTimerId{-1},
    m_spDataFilter{nullptr},
    m_aPlotCurves{}
{
    setFooter("-");

    if (auto pCanvas = dynamic_cast<QwtPlotCanvas *>(canvas()))
    {
        pCanvas->setBorderRadius(10);
    }

    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->setPen(Qt::gray, 0.0, Qt::DotLine);
    grid->enableX(true);
    grid->enableXMin(true);
    grid->enableY(true);
    grid->enableYMin(false);
    grid->attach(this);
}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void MyoPlot::StartTimer(int iIntervalMs)
{
    if (m_iTimerId == -1)
    {
        m_iTimerId = startTimer(iIntervalMs);
    }
}

void MyoPlot::StopTimer()
{
    if (m_iTimerId != -1)
    {
        killTimer(m_iTimerId);
        m_iTimerId = -1;
    }
}

void MyoPlot::SetId(size_t iIndex)
{
    m_iId = iIndex;
}

void MyoPlot::SetFilter(std::shared_ptr<EmgDataFilter> pFilter)
{
    m_spDataFilter = pFilter;
}

void MyoPlot::SetPlotCurves(std::array<QwtPlotCurve *, 8> aPlotCurves)
{
    m_aPlotCurves = aPlotCurves;
}

void MyoPlot::timerEvent(QTimerEvent *pEvent)
{
    Update();
}

void MyoPlot::Update()
{
    bool bUsePlotSeries = true;

    m_spDataFilter->Update();
    QwtSeriesData<QPointF> *pSeries = nullptr;
    if (!m_spDataFilter->empty())
    {
        if (bUsePlotSeries)
        {
            setFooter(std::to_string((*m_spDataFilter)[m_spDataFilter->size() - 1][m_iId]).c_str());
            auto cData = Extract<std::vector<QPointF>>(m_spDataFilter.get(), m_iId);
            pSeries = new PlotSeries<std::vector<QPointF>>(cData);

            for (auto pSignalFilter : m_vecSignalFilters)
            {
                pSignalFilter->SetData(cData, m_iId);
                auto pPlotSeries = new PlotSeries<std::vector<QPointF>>{};
                auto iRange = pSignalFilter->GetEnd(m_iId) - pSignalFilter->GetBegin(m_iId);
                if (iRange > pSignalFilter->ValidBegin())
                {
                    pPlotSeries->SetData(pSignalFilter->GetBegin(m_iId) + pSignalFilter->ValidBegin(),
                                         pSignalFilter->GetEnd(m_iId));
                    pSignalFilter->GetPlotCurve(m_iId)->setData(pPlotSeries);
                }
            }
        }
        else
        {
            QVector<QPointF> vecData{};
            int iCounter = 0;
            for (auto const &emg : *m_spDataFilter)
            {
                vecData.push_back(EmgToPoint(emg, iCounter, m_iId));
            }
            pSeries = new QwtPointSeriesData{vecData};
        }
        if (pSeries)
        {
            double dAxisScale = 250.0;
            double dDiff = pSeries->boundingRect().right() - pSeries->boundingRect().left();
            if (dDiff > 1000.0)
            {
                dAxisScale = 1000.0;
            }
            if (dDiff > 5000.0)
            {
                dAxisScale = 5000.0;
            }
            setAxisScale(xBottom, pSeries->boundingRect().left(), pSeries->boundingRect().right(), dAxisScale);
            m_aPlotCurves[m_iId]->setData(pSeries);
            replot();
        }
    }
}

void MyoPlot::AddSignalFilter(std::shared_ptr<SignalFilter<std::vector<QPointF>>> spSignalFilter)
{
    m_vecSignalFilters.push_back(spSignalFilter);
    spSignalFilter->GetPlotCurve(m_iId)->attach(this);
}

std::vector<std::shared_ptr<SignalFilter<std::vector<QPointF>>>> &MyoPlot::GetSignalFilters()
{
    return m_vecSignalFilters;
}

} // namespace MyoVisualizer
