#include "EmgData.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <algorithm>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
EmgData::EmgData(std::uint64_t uiTimestamp, std::int8_t const *pData):
    m_uiTimestamp{uiTimestamp},
    m_aData{}
{
    std::copy(pData, pData + 8, std::begin(m_aData));
}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

bool EmgData::operator==(EmgData const &rhs) const
{
    return (m_uiTimestamp == rhs.m_uiTimestamp);
}

bool EmgData::operator<(EmgData const &rhs) const
{
    return (m_uiTimestamp < rhs.m_uiTimestamp);
}

std::int8_t const &EmgData::operator[](size_t iSensor) const
{
    return m_aData[iSensor];
}

} // namespace MyoVisualizer
