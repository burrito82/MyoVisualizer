#ifndef MYOVIS_EMGDATA_H
#define MYOVIS_EMGDATA_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <cstdint>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/

namespace MyoVisualizer
{
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
 * @param
 * @return
 * @see
 * @todo
 * @bug
 * @deprecated
 */
struct EmgData
{
public:
    explicit EmgData(std::uint64_t uiTimestamp, std::int8_t const *pData);

    bool operator==(EmgData const &rhs) const;
    bool operator<(EmgData const &rhs) const;
    std::int8_t const &operator[](size_t iSensor) const;

    std::uint64_t m_uiTimestamp;
private:
    std::int8_t m_aData[8];
};

} // namespace MyoVisualizer

#endif // ! MYOVIS_EMGDATA_H

