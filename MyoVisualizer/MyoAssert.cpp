#include "MyoAssert.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void Assert(bool bAssertion, char const *pErrorMsg /*= nullptr*/)
{
    if (!bAssertion)
    {
        throw MyoVisException{pErrorMsg};
    }
}

void Assert(bool bAssertion, std::string const &strErrorMsg)
{
    if (!bAssertion)
    {
        throw MyoVisException{strErrorMsg};
    }
}

} // namespace MyoVisualizer
