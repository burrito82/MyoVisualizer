#include "MyoVisualizer.h"
/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "Classifier.h"
#include "Classifiers/DecisionTree.h"
#include "Classifiers/HardCoded.h"
#include "ComputationPlot.h"
#include "EmgDataFilterIncomingWindow.h"
#include "EmgDataFilterSlidingWindow.h"
#include "MyoListener.h"
#include "MyoPlot.h"
#include "SignalFilters/BandPass.h"
#include "SignalFilters/Extremal.h"
#include "SignalFilters/FullWaveRectifier.h"
#include "SignalFilters/Mean.h"
#include "SignalFilters/Smooth.h"

#include <qwt_plot.h>
#include <qwt_plot_curve.h>

#include <QDir>
#include <QFileDialog>
#include <QKeyEvent>

#include <algorithm>
#include <array>
#include <fstream>
#include <memory>
#include <string>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/
class MyoVisualizer::Impl
{
public:
    std::shared_ptr<SignalFilters::BandPass<std::vector<QPointF>>> m_spBandpassFilter;
};
/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

MyoVisualizer::MyoVisualizer(QWidget *parent):
    QMainWindow{parent},
    m_oMyoControl{},
    m_spListener{nullptr},
    m_oGestureRecorder{},
    m_pClassifier{nullptr},
    m_oGyroscope{},
    m_oMyoSyringe{m_oGyroscope},
    m_iTimerId{-1},
    m_vecRunInGuiThread{},
    m_pImpl{std::make_unique<Impl>()}
{
    ui.setupUi(this);
    
    m_aMyoPlots = std::array<MyoPlot *, 8>
    {
        ui.PlotSensor0, ui.PlotSensor1, ui.PlotSensor2, ui.PlotSensor3,
        ui.PlotSensor4, ui.PlotSensor5, ui.PlotSensor6, ui.PlotSensor7
    };

    m_aButtons = std::array<QPushButton *, 8>
    {
        ui.btn0, ui.btn1, ui.btn2, ui.btn3,
        ui.btn4, ui.btn5, ui.btn6, ui.btn7
    };

    // setup plot curves
    std::array<QwtPlotCurve *, 8> aCurves;
    for (int i = 0; i < 8; ++i)
    {
        m_aMyoPlots[i]->SetId(i);
        m_aMyoPlots[i]->setTitle((std::string{"Sensor "} +std::to_string(i)).c_str());
        m_aMyoPlots[i]->setAutoReplot(false);

        aCurves[i] = new QwtPlotCurve{};
        //aCurves[i]->setStyle(QwtPlotCurve::Dots);
        aCurves[i]->setRenderHint(QwtPlotItem::RenderAntialiased, true);
        //aCurves[i]->setPaintAttribute(QwtPlotCurve::ClipPolygons, false);
        aCurves[i]->setData(new QwtPointSeriesData{});
        aCurves[i]->attach(m_aMyoPlots[i]);
    }

    // computation plot curve
    {
        ui.PlotComputation->setTitle("Computation Plot");
        ui.PlotComputation->setAutoReplot(false);
    }

    m_spListener = std::make_shared<MyoListener>();
    m_oMyoControl.Listen(m_spListener);
    auto spWindowFilter = std::make_shared<EmgDataFilterIncomingWindow>(m_spListener->GetEmgContainerRef());
    spWindowFilter->SetWindowSize(3000);
    m_spViewFilter = spWindowFilter;

    using BandPass = SignalFilters::BandPass<std::vector<QPointF>>;
    using FullWaveRectifier = SignalFilters::FullWaveRectifier<std::vector<QPointF>>;
    using Mean = SignalFilters::Mean<std::vector<QPointF>>;
    using Smooth = SignalFilters::Smooth<std::vector<QPointF>>;
    using Extremal = SignalFilters::Extremal<std::vector<QPointF>>;

    auto spSignalFilter0 = std::make_shared<FullWaveRectifier>();
    auto spSignalFilter1 = std::make_shared<Mean>(spSignalFilter0, 25u);
    //auto spSignalFilter2 = std::make_shared<Mean>(spSignalFilter0, 100u);
    //auto spSignalFilter3 = std::make_shared<Mean>(spSignalFilter0, 50u);
    //auto spSignalFilter4 = std::make_shared<Smooth>(spSignalFilter3, 25u);
    //auto spMinFilter = std::make_shared<Extremal>(spSignalFilter4, Extremal::MIN);
    //auto spMaxFilter = std::make_shared<Extremal>(spSignalFilter4, Extremal::MAX);
    auto spBandpassFilter = std::make_shared<BandPass>(spSignalFilter1);
    m_pImpl->m_spBandpassFilter = spBandpassFilter;
    //spSignalFilter1->SetColor(Qt::yellow);
    spSignalFilter1->SetColor(Qt::darkRed);
    //spSignalFilter2->SetColor(Qt::green);
    //spSignalFilter3->SetColor(Qt::red);
    //spSignalFilter4->SetColor(Qt::darkRed);
    //spMinFilter->SetColor(Qt::darkCyan);
    //spMaxFilter->SetColor(Qt::darkCyan);
    spBandpassFilter->SetColor(Qt::darkGreen);
    for (int i = 0; i < 8; ++i)
    {
        m_aMyoPlots[i]->AddSignalFilter(spSignalFilter0);
        m_aMyoPlots[i]->AddSignalFilter(spSignalFilter1);
        //m_aMyoPlots[i]->AddSignalFilter(spSignalFilter2);
        //m_aMyoPlots[i]->AddSignalFilter(spSignalFilter3);
        //m_aMyoPlots[i]->AddSignalFilter(spSignalFilter4);
        //m_aMyoPlots[i]->AddSignalFilter(spMinFilter);
        //m_aMyoPlots[i]->AddSignalFilter(spMaxFilter);
        m_aMyoPlots[i]->AddSignalFilter(spBandpassFilter);
    }

    for (int i = 0; i < 8; ++i)
    {
        m_aMyoPlots[i]->SetPlotCurves(aCurves);
        m_aMyoPlots[i]->SetFilter(m_spViewFilter);
        m_aMyoPlots[i]->StartTimer(20);
    }
    //ui.PlotComputation->AddSignalFilter(spSignalFilter4);
    ui.PlotComputation->AddSignalFilter(spSignalFilter1);
    ui.PlotComputation->AddSignalFilter(spBandpassFilter);
    ui.PlotComputation->StartTimer(20);

    ui.PlotComputation->AddPlotLine(Qt::darkYellow, [this](ComputationPlot::PlotData_t const &vecInput)
    {
        static std::mutex S_oMutex{};
        std::lock_guard<std::mutex> oLock{S_oMutex};
        static ComputationPlot::PlotData_t vecPlot{};
        if (m_spListener->GetEmgContainerRef().size() == 0 || vecInput.empty())
        {
            return vecPlot;
        }
        auto const uiFirstValidTimestamp = vecInput.front().x();
        ComputationPlot::PlotData_t filtered{};
        std::copy_if(vecPlot.begin(), vecPlot.end(), std::back_inserter(filtered),
                     [uiFirstValidTimestamp](ComputationPlot::PlotData_t::value_type const &value)
        {
            return value.x() >= uiFirstValidTimestamp;
        });
        vecPlot = std::move(filtered);
        auto timestamp = (m_spListener->GetLatestTimestamp() - m_spListener->GetEmgContainerRef()[0].m_uiTimestamp) / 1000.0;
        ComputationPlot::PlotData_t::value_type newPoint{};
        newPoint.setX(timestamp);
        newPoint.setY(m_spListener->GetGyroscope().magnitude());
        vecPlot.push_back(newPoint);
        return vecPlot;
    });

    auto pMainApp = this;
    m_oGestureRecorder.SetGestureName('1', "Syringe");
    ui.PlotComputation->AddPlotLine(Qt::darkGreen, [pMainApp](ComputationPlot::PlotData_t const &vecInput)
    {
        auto const height = 140.0;
        ComputationPlot::PlotData_t vecPlot{};
        ComputationPlot::PlotData_t::value_type newPoint{};
        if (vecInput.empty())
        {
            return vecPlot;
        }

        Gesture::GestureId id = '1';
        auto const firstX = static_cast<std::uint64_t>(vecInput.front().x() * 1000.0);
        auto const lastX = static_cast<std::uint64_t>(vecInput.back().x() * 1000.0);
        auto const &srcGesturesCompleted = pMainApp->m_oGestureRecorder.GetCompletedGestures();
        GestureRecorder::Container_Gesture gesturesCompleted{};
        std::copy_if(std::begin(srcGesturesCompleted), std::end(srcGesturesCompleted), std::back_inserter(gesturesCompleted),
            [firstX, id](Gesture const &g) -> bool
        {
            return (g.Id() == id && (g.Start() >= firstX || g.End() >= firstX));
        });

        if (gesturesCompleted.empty() && pMainApp->m_oGestureRecorder.GetActiveGesture(id) == nullptr)
        {
            newPoint.setX(firstX / 1000.0);
            newPoint.setY(0.0);
            vecPlot.push_back(newPoint);
            newPoint.setX(lastX / 1000.0);
            newPoint.setY(0.0);
            vecPlot.push_back(newPoint);
            return vecPlot;
        }

        auto it = std::begin(gesturesCompleted);
        if (it != std::end(gesturesCompleted) && it->Start() < firstX)
        {
            newPoint.setX(firstX / 1000.0);
            newPoint.setY(height);
            vecPlot.push_back(newPoint);

            newPoint.setX(it->End() / 1000.0);
            newPoint.setY(height);
            vecPlot.push_back(newPoint);

            newPoint.setX((it->End() + 1.0) / 1000.0);
            newPoint.setY(0.0);
            vecPlot.push_back(newPoint);
            gesturesCompleted.erase(it);
        }
        else
        {
            newPoint.setX(firstX / 1000.0);
            newPoint.setY(0.0);
            vecPlot.push_back(newPoint);
        }

        for (it = std::begin(gesturesCompleted);
             it != std::end(gesturesCompleted);
             ++it)
        {
            newPoint.setX((it->Start() - 1.0) / 1000.0);
            newPoint.setY(0.0);
            vecPlot.push_back(newPoint);

            newPoint.setX(it->Start() / 1000.0);
            newPoint.setY(height);
            vecPlot.push_back(newPoint);

            newPoint.setX(it->End() / 1000.0);
            newPoint.setY(height);
            vecPlot.push_back(newPoint);

            newPoint.setX((it->End() + 1.0) / 1000.0);
            newPoint.setY(0.0);
            vecPlot.push_back(newPoint);
        }

        auto pActiveGesture = pMainApp->m_oGestureRecorder.GetActiveGesture(id);
        if (pActiveGesture != nullptr)
        {
            newPoint.setX((pActiveGesture->Start() - 1) / 1000.0);
            newPoint.setY(0.0);
            vecPlot.push_back(newPoint);
            newPoint.setX(pActiveGesture->Start() / 1000.0);
            newPoint.setY(height);
            vecPlot.push_back(newPoint);
            newPoint.setX(lastX / 1000.0);
            newPoint.setY(height);
            vecPlot.push_back(newPoint);
        }
        else
        {
            newPoint.setX(lastX / 1000.0);
            newPoint.setY(0.0);
            vecPlot.push_back(newPoint);
        }

        return vecPlot;
    });
    //ui.PlotComputation->AddClassifier(Qt::darkYellow, new Classifiers::HardCoded{});
    //ui.PlotComputation->AddClassifier(Qt::darkBlue, new Classifiers::DecisionTree{});

    connect(&m_spListener->GetEmgContainerRef(), SIGNAL(EmgDataAdded()), this, SLOT(on_EmgDataAdded()));
    connect(ui.PlotComputation, SIGNAL(on_value_changed(QString)), ui.lblComp0, SLOT(setText(QString)));
    connect(ui.PlotComputation, SIGNAL(on_valueGradient_changed(QString)), ui.lblComp1, SLOT(setText(QString)));
    //connect(ui.PlotComputation, SIGNAL(on_valueGradient_changed(float)), ui.lblComp1, SLOT(setValue(float)));
    //connect(ui.PlotComputation, SIGNAL(on_variance_changed(float)), ui.lblComp2, SLOT(setValue(float)));
    //connect(ui.PlotComputation, SIGNAL(on_varianceGradient_changed(float)), ui.lblComp3, SLOT(setValue(float)));
    connect(ui.PlotComputation, SIGNAL(on_statusMessage_changed(QString)), ui.lblComp2, SLOT(setText(QString)));

    ui.lblAmpel->setText("");
    m_spListener->SetOnGyroscopeData([this](myo::Vector3<float> const &gyro)
    {
        m_oGyroscope.Log(gyro.magnitude(), m_spListener->GetLatestTimestamp());
        float const gyroMag = gyro.magnitude();
        /*RunInGuiThread([this, gyroMag]()
        {
            ui.lblAmpel->setText(QString::number(gyroMag));
        });*/
    });
    m_oGyroscope.SetOnArmStable([this](float fValue, std::uint64_t)
    {
        RunInGuiThread([this, fValue]()
        {
            ui.lblAmpel->setStyleSheet("QLabel { background-color : green; color : white; }");
        });
    });
    m_oGyroscope.SetOnArmStabilizing([this](float fValue, std::uint64_t)
    {
        RunInGuiThread([this, fValue]()
        {
            ui.lblAmpel->setStyleSheet("QLabel { background-color : yellow; color : black; }");
        });
    });
    m_oGyroscope.SetOnArmUnstable([this](float fValue, std::uint64_t)
    {
        RunInGuiThread([this, fValue]()
        {
            ui.lblAmpel->setStyleSheet("QLabel { background-color : red; color : white; }");
        });
    });
    m_oMyoSyringe.SetOnSyringeBegin([this]()
    {
        ui.lblMyoSyringeStatus->setText("drug admission in progress...");
    });
    m_oMyoSyringe.SetOnSyringeEnd([this](std::uint64_t uiAdmissionDuration)
    {
        ui.lblMyoSyringeStatus->setText("drug admission in finished in "
                                        + QString::number(static_cast<float>(uiAdmissionDuration) / 1000000.0f) + " seconds!");
    });
    StartTimer();
}

MyoVisualizer::~MyoVisualizer()
{
    StopTimer();
    m_oMyoControl.StopListening();

    for (int i = 0; i < 8; ++i)
    {
        m_aMyoPlots[i]->StopTimer();
    }

    std::string const strDirectory = "samples";
    if (!QDir{QString::fromStdString(strDirectory)}.exists())
    {
        QDir{}.mkdir(QString::fromStdString(strDirectory));
    }
    std::string const strTimeNow = std::to_string(std::chrono::system_clock::now().time_since_epoch().count());
    std::string strFilepath = strDirectory + '/' + strTimeNow;
    {
        std::ofstream oGesFile{strFilepath + ".csv"};
        oGesFile << m_spListener->GetEmgContainerRef();
    }

    auto const &names = m_oGestureRecorder.GetGestureNames();
    for (auto const &name : names)
    {
        std::ofstream oCsvFile{strFilepath + '.' + name.second + ".ges"};
        m_oGestureRecorder.Export(oCsvFile, name.first);
    }

    {
        std::ofstream oGyroFile{strFilepath + ".gyro.csv"};
        oGyroFile << m_oGyroscope;
    }

    {
        std::ofstream oSyringeFile{strFilepath + ".syringe.csv"};
        oSyringeFile << m_oMyoSyringe;
    }
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void MyoVisualizer::on_btn0_clicked()
{
    Show(0);
}
void MyoVisualizer::on_btn1_clicked()
{
    Show(1);
}
void MyoVisualizer::on_btn2_clicked()
{
    Show(2);
}
void MyoVisualizer::on_btn3_clicked()
{
    Show(3);
}
void MyoVisualizer::on_btn4_clicked()
{
    Show(4);
}
void MyoVisualizer::on_btn5_clicked()
{
    Show(5);
}
void MyoVisualizer::on_btn6_clicked()
{
    Show(6);
}
void MyoVisualizer::on_btn7_clicked()
{
    Show(7);
}

void MyoVisualizer::on_actionConnect_triggered()
{
    m_oMyoControl.Listen(m_spListener);
}

void MyoVisualizer::on_actionDisconnect_triggered()
{
    m_oMyoControl.StopListening();
}

void MyoVisualizer::on_actionClear_triggered()
{
    m_oMyoControl.StopListening();
    for (auto &pPlot : m_aMyoPlots)
    {
        pPlot->StopTimer();
    }
    ui.PlotComputation->StopTimer();
    m_spListener->GetEmgContainerRef().clear();
    m_oGestureRecorder.clear();
    m_spViewFilter->Update();
    for (auto &pPlot : m_aMyoPlots)
    {
        pPlot->StartTimer();
    }
    ui.PlotComputation->StartTimer();
    m_oMyoControl.Listen(m_spListener);
}

void MyoVisualizer::on_actionLoad_EMG_data_from_csv_triggered()
{
    m_oMyoControl.StopListening();
    for (auto &pPlot : m_aMyoPlots)
    {
        pPlot->StopTimer();
    }
    ui.PlotComputation->StopTimer();
    m_spListener->GetEmgContainerRef().clear();
    m_oGestureRecorder.clear();
    {
        std::string strFilepath = QFileDialog::getOpenFileName(nullptr, "Load from csv", QString{}, "*.csv").toStdString();
        std::ifstream oCsvFile{strFilepath};
        oCsvFile >> m_spListener->GetEmgContainerRef();
    }
    m_spViewFilter->Update();
    for (auto &pPlot : m_aMyoPlots)
    {
        pPlot->StartTimer();
    }
    ui.PlotComputation->StartTimer();
}

void MyoVisualizer::on_actionSave_to_csv_triggered()
{
    std::string strFilepath = QFileDialog::getSaveFileName(this, "Save to csv", QString{}, "*.csv").toStdString();
    std::ofstream oCsvFile{strFilepath};
    oCsvFile << m_spListener->GetEmgContainerRef();
}

void MyoVisualizer::on_actionLoad_Gesture_data_triggered()
{
    std::string strFilepath = QFileDialog::getOpenFileName(nullptr, "Load from ges", QString{}, "*.ges").toStdString();
    std::ifstream oGesFile{strFilepath};
    m_oGestureRecorder.clear();
    m_oGestureRecorder.Import(oGesFile);
}

void MyoVisualizer::on_actionExport_All_triggered()
{
    using FullWaveRectifier = SignalFilters::FullWaveRectifier<std::vector<QPointF>>;
    using Mean = SignalFilters::Mean<std::vector<QPointF>>;
    using Smooth = SignalFilters::Smooth<std::vector<QPointF>>;
    using Extremal = SignalFilters::Extremal<std::vector<QPointF>>;

    auto const &emgContainer = m_spListener->GetEmgContainerRef();
    std::vector<QPointF> vecData(emgContainer.size());
    auto spSignalFilter0 = std::make_shared<FullWaveRectifier>();
    auto spSignalFilter3 = std::make_shared<Mean>(spSignalFilter0, 50u);
    auto spSignalFilter4 = std::make_shared<Smooth>(spSignalFilter3, 25u);
    for (auto iSensor = 0u; iSensor < 8u; ++iSensor)
    {
        std::transform(std::begin(emgContainer), std::end(emgContainer), std::begin(vecData),
                       [iSensor](EmgData const &emg)
        {
            return QPointF(emg.m_uiTimestamp, emg[iSensor]);
        });
        spSignalFilter4->SetData(vecData, iSensor);
    }
    std::vector<QPointF> vecEmg[8];
    for (auto iSensor = 0u; iSensor < 8u; ++iSensor)
    {
        for (auto it = spSignalFilter4->GetBegin(iSensor); it != spSignalFilter4->GetEnd(iSensor); ++it)
        {
            vecEmg[iSensor].push_back(*it);
        }
    }
    auto iSize = std::min(std::min(std::min(vecEmg[0].size(), vecEmg[1].size()), vecEmg[2].size()), vecEmg[3].size());
    iSize = std::min(std::min(std::min(std::min(iSize, vecEmg[4].size()), vecEmg[5].size()), vecEmg[6].size()), vecEmg[7].size());
    // timestamp | value | label | diff | diff10 | value[-5] | value[+5] | value[-25] | value[+25] | value[-100] | value[+100]
    std::vector<std::tuple<float, float, std::string, float, float, float, float, float, float, float, float>> vecAvgEmg(iSize);
    auto itGesture = m_oGestureRecorder.begin();
    for (auto i = 0u; i < iSize; ++i)
    {
        auto x = vecEmg[0][i].x();
        std::get<0>(vecAvgEmg[i]) = x;
        double avg = 0.0;
        for (auto iSensor = 0u; iSensor < 8u; ++iSensor)
        {
            avg += vecEmg[iSensor][i].y();
        }
        std::get<1>(vecAvgEmg[i]) = avg / 8.0;
        std::get<2>(vecAvgEmg[i]) = "INACTIVE";
        if (itGesture != std::end(m_oGestureRecorder))
        {
            if (itGesture->Start() <= x)
            {
                if (itGesture->End() > x)
                {
                    int zone = 50;
                    if (itGesture->Start() + zone > x && itGesture->End() - zone > x)
                    {
                        std::get<2>(vecAvgEmg[i]) = "ACTIVATING";
                    }
                    else
                    {
                        if (itGesture->Start() + zone < x && itGesture->End() - zone <= x)
                        {
                            std::get<2>(vecAvgEmg[i]) = "DEACTIVATING";
                        }
                        else
                        {
                            std::get<2>(vecAvgEmg[i]) = "ACTIVE";
                        }
                    }
                }
                else
                {
                    ++itGesture;
                }
            }
        }
    }
    for (auto i = 1u; i < vecAvgEmg.size() - 1u; ++i)
    {
        auto x0 = std::get<0>(vecAvgEmg[i - 1u]);
        auto x2 = std::get<0>(vecAvgEmg[i + 1u]);
        auto y0 = std::get<1>(vecAvgEmg[i - 1u]);
        auto y2 = std::get<1>(vecAvgEmg[i + 1u]);
        std::get<3>(vecAvgEmg[i]) = (y2 - y0) / (x2 - x0);
    }
    for (auto i = 10u; i < vecAvgEmg.size() - 10u; ++i)
    {
        auto x0 = std::get<0>(vecAvgEmg[i - 10u]);
        auto x2 = std::get<0>(vecAvgEmg[i + 10u]);
        auto y0 = std::get<1>(vecAvgEmg[i - 10u]);
        auto y2 = std::get<1>(vecAvgEmg[i + 10u]);
        std::get<4>(vecAvgEmg[i]) = (y2 - y0) / (x2 - x0);
    }
    for (auto i = 5; i < vecAvgEmg.size(); ++i)
    {
        std::get<5>(vecAvgEmg[i]) = std::get<1>(vecAvgEmg[i - 5]);
    }
    for (auto i = 0; i < vecAvgEmg.size() - 5; ++i)
    {
        std::get<6>(vecAvgEmg[i]) = std::get<1>(vecAvgEmg[i + 5]);
    }
    for (auto i = 25; i < vecAvgEmg.size(); ++i)
    {
        std::get<7>(vecAvgEmg[i]) = std::get<1>(vecAvgEmg[i - 25]);
    }
    for (auto i = 0; i < vecAvgEmg.size() - 25; ++i)
    {
        std::get<8>(vecAvgEmg[i]) = std::get<1>(vecAvgEmg[i + 25]);
    }
    for (auto i = 100; i < vecAvgEmg.size(); ++i)
    {
        std::get<9>(vecAvgEmg[i]) = std::get<1>(vecAvgEmg[i - 100]);
    }
    for (auto i = 0; i < vecAvgEmg.size() - 100; ++i)
    {
        std::get<10>(vecAvgEmg[i]) = std::get<1>(vecAvgEmg[i + 100]);
    }
    std::string strFilepath = QFileDialog::getSaveFileName(this, "Save to csv", QString{}, "*.csv").toStdString();
    std::ofstream oCsvFile{strFilepath};
    oCsvFile << "avg; label; diff; diff10; avgtm5; avgtp5; avgtm25; avgtp25; avgtm100; avgtp100\n";
    for (auto const &t : vecAvgEmg)
    {
        oCsvFile //<< std::get<0>(t) << "; "
            << std::get<1>(t) << "; "
            << std::get<2>(t) << "; "
            << std::get<3>(t) << "; "
            << std::get<4>(t) << "; "
            << std::get<5>(t) << "; "
            << std::get<6>(t) << "; "
            << std::get<7>(t) << "; "
            << std::get<8>(t) << "; "
            << std::get<9>(t) << "; "
            << std::get<10>(t) << "\n";
    }
}

void MyoVisualizer::on_actionUpdate_Window_triggered()
{
    if (ui.actionUpdate_Window->isChecked())
    {
        int iWindowSize = std::max(10, ui.windowSizeSbox->value()) - 1;
        auto spFilter = std::make_shared<EmgDataFilterIncomingWindow>(m_spListener->GetEmgContainerRef());
        spFilter->SetWindowSize(iWindowSize);
        m_spViewFilter = spFilter;
        for (int i = 0; i < 8; ++i)
        {
            m_aMyoPlots[i]->SetFilter(m_spViewFilter);
        }
    }
    else
    {
        on_windowSlider_sliderMoved();
    }
}

void MyoVisualizer::on_windowSlider_sliderMoved()
{
    int iWindowSize = std::max(10, ui.windowSizeSbox->value()) - 1;
    ui.actionUpdate_Window->setChecked(false);
    auto spFilter = std::make_shared<EmgDataFilterSlidingWindow>(m_spListener->GetEmgContainerRef());
    spFilter->SetBegin(ui.windowSlider->value());
    spFilter->SetWindowSize(iWindowSize);
    m_spViewFilter = spFilter;
    for (int i = 0; i < 8; ++i)
    {
        m_aMyoPlots[i]->SetFilter(m_spViewFilter);
    }
}

void MyoVisualizer::on_windowSizeSlider_sliderMoved()
{
    int iValue = std::max(10, ui.windowSizeSlider->value());
    ui.windowSizeSbox->setValue(iValue);
    auto spFilter = m_spViewFilter;
    if (auto pFilter = dynamic_cast<EmgDataFilterIncomingWindow *>(spFilter.get()))
    {
        pFilter->SetWindowSize(iValue - 1);
    }
    if (auto pFilter = dynamic_cast<EmgDataFilterSlidingWindow *>(spFilter.get()))
    {
        pFilter->SetWindowSize(iValue - 1);
    }
}

void MyoVisualizer::on_windowSizeSbox_valueChanged()
{
    int iValue = std::max(10, ui.windowSizeSbox->value());
    ui.windowSizeSlider->setValue(iValue);
    auto spFilter = m_spViewFilter;
    if (auto pFilter = dynamic_cast<EmgDataFilterIncomingWindow *>(spFilter.get()))
    {
        pFilter->SetWindowSize(iValue - 1);
    }
    if (auto pFilter = dynamic_cast<EmgDataFilterSlidingWindow *>(spFilter.get()))
    {
        pFilter->SetWindowSize(iValue - 1);
    }
}

void MyoVisualizer::on_spSyringeVolume_valueChanged()
{
    ui.lineTime->setText(QString::number(ui.spSyringeVolume->value() / ui.spAdmissionRate->value()));
}

void MyoVisualizer::on_spAdmissionRate_valueChanged()
{
    ui.lineTime->setText(QString::number(ui.spSyringeVolume->value() / ui.spAdmissionRate->value()));
}

void MyoVisualizer::on_cbLowPass_stateChanged()
{
    double const dLow = ui.dbSbLowPass->value();
    double const dHigh = ui.dbSbHighPass->value();
    bool const bLow = ui.cbLowPass->isChecked();
    bool const bHigh = ui.cbHighPass->isChecked();
    if (bLow)
    {
        if (bHigh)
        {
            m_pImpl->m_spBandpassFilter->SetBandPass(dLow, dHigh);
        }
        else
        {
            m_pImpl->m_spBandpassFilter->SetLowPass(dLow);
        }
    }
    else
    {
        if (bHigh)
        {
            m_pImpl->m_spBandpassFilter->SetHighPass(dHigh);
        }
        else
        {
            m_pImpl->m_spBandpassFilter->SetPassthrough();
        }
    }
}

void MyoVisualizer::on_cbHighPass_stateChanged()
{
    on_cbLowPass_stateChanged();
}

void MyoVisualizer::on_dbSbLowPass_valueChanged()
{
    on_cbLowPass_stateChanged();
}

void MyoVisualizer::on_dbSbHighPass_valueChanged()
{
    on_cbLowPass_stateChanged();
}

void MyoVisualizer::on_EmgDataAdded()
{
    int iWindowSize = std::max(10, ui.windowSizeSbox->value()) - 1;
    ui.windowSlider->setMaximum(std::max<int>(iWindowSize, m_spListener->GetEmgContainerRef().size()));
    ui.windowSlider->setValue(m_spViewFilter->From());
}

void MyoVisualizer::keyPressEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat() == false && m_spListener != nullptr)
    {
        m_oGestureRecorder.SetCurrentTimestamp(m_spListener->GetLatestTimestamp());
        m_oGestureRecorder.RecordGestureStart(event->key());
        auto const &rGestureNames = m_oGestureRecorder.GetGestureNames();
        char key = static_cast<char>(0xff & event->key());
        std::string strGestureName{key};
        auto it = rGestureNames.find(key);
        if (it != std::end(rGestureNames))
        {
            strGestureName = it->second;
            ui.lblComp4->setText(QString::fromStdString(std::string{"record gesture start for "} + strGestureName));
        }
    }
}

void MyoVisualizer::keyReleaseEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat() == false && m_spListener != nullptr)
    {
        m_oGestureRecorder.SetCurrentTimestamp(m_spListener->GetLatestTimestamp());
        m_oGestureRecorder.RecordGestureEnd(event->key());
        auto const &rGestureNames = m_oGestureRecorder.GetGestureNames();
        char key = static_cast<char>(0xff & event->key());
        std::string strGestureName{key};
        auto it = rGestureNames.find(key);
        if (it != std::end(rGestureNames))
        {
            strGestureName = it->second;
            ui.lblComp4->setText(QString::fromStdString(std::string{"record gesture end for "} + strGestureName));
        }
    }
}

void MyoVisualizer::Show(size_t iSensor)
{
    if (m_aButtons[iSensor]->text() == "Fill")
    {
        m_aMyoPlots[iSensor]->show();
        for (int i = 0; i < 8; ++i)
        {
            if (i != iSensor)
            {
                m_aMyoPlots[i]->hide();
            }
            m_aButtons[i]->setText("Fill");
        }
        ui.PlotComputation->hide();
        m_aButtons[iSensor]->setText("Show all");
    }
    else
    {
        for (int i = 0; i < 8; ++i)
        {
            m_aMyoPlots[i]->show();
            m_aButtons[i]->setText("Fill");
        }
        ui.PlotComputation->show();
    }
}

void MyoVisualizer::timerEvent(QTimerEvent *pEvent)
{
    GuiThread();
}

void MyoVisualizer::StartTimer()
{
    int const iIntervalMs = 10;
    if (m_iTimerId == -1)
    {
        m_iTimerId = startTimer(iIntervalMs);
    }
}

void MyoVisualizer::StopTimer()
{
    if (m_iTimerId != -1)
    {
        killTimer(m_iTimerId);
        m_iTimerId = -1;
    }
}

void MyoVisualizer::GuiThread()
{
    decltype(m_vecRunInGuiThread) vecFuncs{};
    {
        std::lock_guard<std::mutex> oLock{m_oGuiFuncsMutex};
        vecFuncs = std::move(m_vecRunInGuiThread);
    }
    for (auto &func : vecFuncs)
    {
        func();
    }

    m_oMyoSyringe.SetCurrentEmg(ui.PlotComputation->GetLatestValue(), ui.PlotComputation->GetLatestTimestamp());
    if (m_oGyroscope.GetState() == Gyroscope::ARM_STABLE
        && m_oMyoSyringe.GetState() == MyoSyringe::ADMINISTRATION_IN_PROGRESS)
    {
        float const fDurationSec = static_cast<float>(m_oMyoSyringe.GetNow() - m_oMyoSyringe.GetSyringeBegin()) / 1000000.0f;
        float const fAdmissionRate = ui.spAdmissionRate->value();
        float const fDrugVolume = ui.spSyringeVolume->value();
        float const fCurVolume = std::min(fDurationSec * fAdmissionRate, fDrugVolume);
        auto const fPercentage = 100.0f * fCurVolume / fDrugVolume;
        ui.lblMyoSyringeStatus->setText("MyoSyringe: " + QString::number(fPercentage) + "%");
        ui.progressBar->setValue(static_cast<int>(fPercentage));
        ui.lblAmpel->setText(QString::number(static_cast<int>(std::ceil(fDrugVolume - fCurVolume))) + " ml left");
    }
    else
    {
        switch (m_oGyroscope.GetState())
        {
        case Gyroscope::ARM_UNSTABLE:
        {
            ui.progressBar->setValue(0);
            ui.lblAmpel->setText("please stabilize\nyour arm");
            break;
        }
        case Gyroscope::ARM_STABILIZING:
        {
            ui.progressBar->setValue(0);
            ui.lblAmpel->setText("good...");
            break;
        }
        case Gyroscope::ARM_STABLE:
        {
            ui.lblAmpel->setText("start the drug\nadmission");
            break;
        }
        }
    }
}

void MyoVisualizer::RunInGuiThread(std::function<void()> func)
{
    std::lock_guard<std::mutex> oLock{m_oGuiFuncsMutex};
    m_vecRunInGuiThread.push_back(std::move(func));
}

} // namespace MyoVisualizer
