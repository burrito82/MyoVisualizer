#include "GestureRecorder.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void GestureRecorder::SetGestureName(GestureId gestureId, std::string const &strName)
{
    m_mapGestureNames[gestureId] = strName;
}

std::map<GestureRecorder::GestureId, std::string> const &GestureRecorder::GetGestureNames() const
{
    return m_mapGestureNames;
}

void GestureRecorder::RecordGestureStart(GestureId const &gestureId)
{
    if (m_mapUnfinishedGestures.find(gestureId) == std::end(m_mapUnfinishedGestures))
    {
        m_mapUnfinishedGestures.insert(std::make_pair(gestureId, Gesture{gestureId, m_uiCurrentTimestamp}));
    }
}

void GestureRecorder::RecordGestureEnd(GestureId const &gestureId)
{
    auto it = m_mapUnfinishedGestures.find(gestureId);
    if (it == std::end(m_mapUnfinishedGestures))
    {
        return;
    }
    auto gesture = it->second;
    m_mapUnfinishedGestures.erase(it);
    gesture.RecordEnd(m_uiCurrentTimestamp);
    m_deqCompleteGestures.push_back(std::move(gesture));
}

void GestureRecorder::SetCurrentTimestamp(std::uint64_t uiCurrentTimestamp)
{
    m_uiCurrentTimestamp = uiCurrentTimestamp;
}

bool GestureRecorder::GestureActive(GestureId gestureId) const
{
    return (m_mapUnfinishedGestures.find(gestureId) != std::end(m_mapUnfinishedGestures));
}

GestureRecorder::Container_Gesture const &GestureRecorder::GetCompletedGestures() const
{
    return m_deqCompleteGestures;
}

Gesture const *GestureRecorder::GetActiveGesture(GestureId id) const
{
    auto it = m_mapUnfinishedGestures.find(id);
    if (it != std::end(m_mapUnfinishedGestures))
    {
        return &it->second;
    }
    return nullptr;
}

GestureRecorder::const_iterator GestureRecorder::begin() const
{
    return std::begin(m_deqCompleteGestures);
}

GestureRecorder::const_iterator GestureRecorder::end() const
{
    return std::end(m_deqCompleteGestures);
}

void GestureRecorder::clear()
{
    m_deqCompleteGestures.clear();
}

void GestureRecorder::Import(std::istream &is)
{
    std::string strGestureName{};
    is >> strGestureName;
    GestureId id{};

    for (auto const &item : m_mapGestureNames)
    {
        if (item.second == strGestureName)
        {
            id = item.first;
            break;
        }
    }
    if (id == GestureId{})
    {
        return;
    }

    while (is)
    {
        std::uint64_t uiStartTime{};
        std::uint64_t uiEndTime{};
        is >> uiStartTime >> uiEndTime;
        m_deqCompleteGestures.push_back(Gesture::Construct(uiStartTime, uiEndTime, id));
    }
}

void GestureRecorder::Export(std::ostream &os, std::string const &strGestureName) const
{
    for (auto const &item : m_mapGestureNames)
    {
        if (item.second == strGestureName)
        {
            Export(os, item.first);
            break;
        }
    }
}

void GestureRecorder::Export(std::ostream &os, GestureId gestureId) const
{
    auto it = m_mapGestureNames.find(gestureId);
    if (it == std::end(m_mapGestureNames))
    {
        return;
    }
    os << it->second << '\n';
    for (auto const &g : m_deqCompleteGestures)
    {
        if (g.Id() == gestureId)
        {
            os << g.Start() << ' ' << g.End() << '\n';
        }
    }
}

std::ostream &operator<<(std::ostream &os, GestureRecorder::Container_Gesture &container)
{
    return os;
}

std::istream &operator>>(std::istream &is, GestureRecorder::Container_Gesture &container)
{
    return is;
}

} // namespace MyoVisualizer
