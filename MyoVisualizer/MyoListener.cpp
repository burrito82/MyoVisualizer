#include "MyoListener.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/

/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
MyoListener::MyoListener():
    m_oDataContainer{},
    m_uiLatestTimestamp{},
    m_funcOnOrientationData{},
    m_funcOnAccelerometerData{},
    m_funcOnGyroscopeData{}
{

}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/
    
void MyoListener::onEmgData(myo::Myo *myo, std::uint64_t timestamp, std::int8_t const *emg)
{
    static std::uint64_t S_iFirstTimestamp = timestamp;
    timestamp -= S_iFirstTimestamp;

    m_oDataContainer.Add(EmgData(timestamp, emg));
    m_uiLatestTimestamp = timestamp;
}

void MyoListener::onOrientationData(myo::Myo *myo, std::uint64_t timestamp, myo::Quaternion<float> const &rotation)
{
    m_qOrientation = rotation;
    if (m_funcOnOrientationData)
    {
        m_funcOnOrientationData(rotation);
    }
}

void MyoListener::onAccelerometerData(myo::Myo *myo, std::uint64_t timestamp, myo::Vector3<float> const &accel)
{
    m_v3Acceleration = accel;
    if (m_funcOnAccelerometerData)
    {
        m_funcOnAccelerometerData(accel);
    }
}

void MyoListener::onGyroscopeData(myo::Myo *myo, std::uint64_t timestamp, myo::Vector3<float> const &gyro)
{
    m_v3Gyroscope = gyro;
    if (m_funcOnGyroscopeData)
    {
        m_funcOnGyroscopeData(gyro);
    }
}

EmgDataContainer const &MyoListener::GetEmgContainerRef() const
{
    return m_oDataContainer;
}

EmgDataContainer &MyoListener::GetEmgContainerRef()
{
    return m_oDataContainer;
}

std::uint64_t MyoListener::GetLatestTimestamp() const
{
    return m_uiLatestTimestamp;
}

void MyoListener::SetOnOrientationData(std::function<void(myo::Quaternion<float> const &)> oFunction)
{
    m_funcOnOrientationData = oFunction;
}

void MyoListener::SetOnAccelerometerData(std::function<void(myo::Vector3<float> const &)> oFunction)
{
    m_funcOnAccelerometerData = oFunction;
}

void MyoListener::SetOnGyroscopeData(std::function<void(myo::Vector3<float> const &)> oFunction)
{
    m_funcOnGyroscopeData = oFunction;
}

myo::Quaternion<float> MyoListener::GetOrientation() const
{
    return m_qOrientation;
}

myo::Vector3<float> MyoListener::GetAcceleration() const
{
    return m_v3Acceleration;
}

myo::Vector3<float> MyoListener::GetGyroscope() const
{
    return m_v3Gyroscope;
}

} // namespace MyoVisualizer
