#include "EmgDataFilterIncomingWindow.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <algorithm>
#include <iterator>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

EmgDataFilterIncomingWindow::EmgDataFilterIncomingWindow(EmgDataContainer const &rData):
    EmgDataFilter{rData},
    m_iWindowSize{0},
    begin_{std::begin(rData)},
    end_{std::begin(rData)}
{

}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void EmgDataFilterIncomingWindow::SetWindowSize(size_t iWindowSize)
{
    m_iWindowSize = iWindowSize;
    Update();
}

size_t EmgDataFilterIncomingWindow::GetWindowSize() const
{
    return m_iWindowSize;
}

EmgDataFilterIncomingWindow::Container<EmgData>::const_iterator EmgDataFilterIncomingWindow::beginImpl() const
{
    return begin_;
}

EmgDataFilterIncomingWindow::Container<EmgData>::const_iterator EmgDataFilterIncomingWindow::endImpl() const
{
    return end_;
}

void EmgDataFilterIncomingWindow::UpdateImpl()
{
    auto const realBegin = std::begin(m_rDataContainer);
    begin_ = std::end(m_rDataContainer);
    end_ = std::end(m_rDataContainer);
    if (begin_ == realBegin)
    {
        return;
    }
    --begin_;
    while ((begin_ != realBegin)
           && ((end_ - 1)->m_uiTimestamp - begin_->m_uiTimestamp < m_iWindowSize * 1000))
    {
        --begin_;
    }
}

} // namespace MyoVisualizer
