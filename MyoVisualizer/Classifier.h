#ifndef MYOVIS_CLASSIFIER_H
#define MYOVIS_CLASSIFIER_H

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <vector>
/*============================================================================*/
/* DEFINES                                                                    */
/*============================================================================*/

/*============================================================================*/
/* FORWARD DECLARATIONS                                                       */
/*============================================================================*/
class QPointF;
namespace MyoVisualizer
{
struct EmgData;
struct Gesture;
/*============================================================================*/
/* STRUCT DEFINITIONS                                                         */
/*============================================================================*/

/*============================================================================*/
/* CLASS DEFINITIONS                                                          */
/*============================================================================*/

/**
* @param
* @return
* @see
* @todo
* @bug
* @deprecated
*/
class IClassifier
{
public:
    virtual ~IClassifier() = default;

    virtual void Train(std::vector<EmgData> const &vecEmgs, std::vector<Gesture> const &vecGestures) = 0;
    virtual float Confidence() const = 0;
    virtual std::vector<Gesture> Classify(std::vector<EmgData> const &vecEmgs) = 0;
    virtual std::vector<Gesture> Classify(std::vector<QPointF> const &vecProcessed) = 0;
};

} // namespace MyoVisualizer

#endif // ! MYOVIS_CLASSIFIER_H

