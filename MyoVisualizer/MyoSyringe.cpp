#include "MyoSyringe.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include "Gyroscope.h"

#include <algorithm>
#include <iostream>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/
MyoSyringe::MyoSyringe(Gyroscope const &oGyroscope):
    m_oGyroscope(oGyroscope),
    m_fLastValue{},
    m_fValueAverage{10.0f},
    m_fWaitingValueAverage{},
    m_eState{},
    m_funcOnSyringeBegin{},
    m_uiNow{},
    m_funcOnSyringeEnd{},
    m_uiSyringeBegin{},
    m_uiSyringeEnd{},
    m_fMaxAdmValue{}
{

}
/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void MyoSyringe::SetCurrentEmg(float fValue, std::uint64_t uiTimestamp)
{
    auto const eGyroState = m_oGyroscope.GetState();

    float const fBeginThreshold = 4.0f;
    //float const fDiff = fValue - m_fLastValue;
    float const fDiff = fValue - m_fValueAverage;
    bool const bBeginThreshold = fDiff > fBeginThreshold;
    bool const bStartMeasuring = bBeginThreshold
        && m_eState == WAITING
        && eGyroState == Gyroscope::ARM_STABLE
        && (uiTimestamp - m_uiSyringeBegin) > 1000000 // ignore event duplicates
        && (uiTimestamp - m_uiSyringeEnd) > 5000000; // leave some time between measurements

    bool const bEndThreshold = fValue < 0.5f * m_fMaxAdmValue;
    std::cout << fValue << " vs " << m_fMaxAdmValue << "             \r";
    std::cout.flush();
    bool const bStopMeasuring = m_eState == ADMINISTRATION_IN_PROGRESS
        && (bEndThreshold
        || eGyroState != Gyroscope::ARM_STABLE)
        && (uiTimestamp - m_uiSyringeBegin) > 500000;

    if (bStartMeasuring)
    {
        m_uiSyringeBegin = uiTimestamp;
        m_fMaxAdmValue = fValue;
        m_fWaitingValueAverage = m_fValueAverage;
        m_eState = ADMINISTRATION_IN_PROGRESS;
        if (static_cast<bool>(m_funcOnSyringeBegin))
        {
            m_funcOnSyringeBegin();
        }
        LogToHistory(uiTimestamp);
    }

    if (m_eState == ADMINISTRATION_IN_PROGRESS)
    {
        m_fMaxAdmValue = std::max(m_fMaxAdmValue, fValue);
    }

    if (bStopMeasuring)
    {
        m_uiSyringeEnd = uiTimestamp;
        m_eState = WAITING;
        m_fMaxAdmValue = 0.0f;
        if (static_cast<bool>(m_funcOnSyringeEnd))
        {
            m_funcOnSyringeEnd(m_uiSyringeEnd - m_uiSyringeBegin);
        }
        m_fValueAverage = m_fWaitingValueAverage;
        LogToHistory(uiTimestamp);
    }

    m_fLastValue = fValue;
    if (m_oGyroscope.GetState() == Gyroscope::ARM_STABLE
        || m_oGyroscope.GetState() == Gyroscope::ARM_STABILIZING)
    {
        float const fAlpha = 0.95f;
        m_fValueAverage = fAlpha * m_fValueAverage + (1.0f - fAlpha) * fValue;
    }
    m_uiNow = uiTimestamp;
}

MyoSyringe::SyringeState MyoSyringe::GetState() const
{
    return m_eState;
}
std::uint64_t MyoSyringe::GetSyringeBegin() const
{
    return m_uiSyringeBegin;
}

std::uint64_t MyoSyringe::GetNow() const
{
    return m_uiNow;
}

std::uint64_t MyoSyringe::GetSyringeEnd() const
{
    return m_uiSyringeEnd;
}

void MyoSyringe::SetOnSyringeBegin(std::function<void()> funcOnSyringeBegin)
{
    m_funcOnSyringeBegin = std::move(funcOnSyringeBegin);
}

void MyoSyringe::SetOnSyringeEnd(std::function<void(std::uint64_t uiAdmissionDuration)> funcOnSyringeEnd)
{
    m_funcOnSyringeEnd = std::move(funcOnSyringeEnd);
}

void MyoSyringe::LogToHistory(std::uint64_t uiTimestamp)
{
    m_vecHistory.push_back(std::make_pair(uiTimestamp, m_eState));
}

void MyoSyringe::Export(std::ostream &os) const
{
    for (auto const &log : m_vecHistory)
    {
        os << log.first << ";" << static_cast<int>(log.second) << "\n";
    }
}

std::ostream &operator<<(std::ostream &os, MyoSyringe const &gyro)
{
    gyro.Export(os);
    return os;
}

} // namespace MyoVisualizer
