#include "EmgDataContainer.h"

/*============================================================================*/
/* INCLUDES                                                                   */
/*============================================================================*/
#include <sstream>
/*============================================================================*/
/* MACROS AND DEFINES, CONSTANTS AND STATICS                                  */
/*============================================================================*/
namespace MyoVisualizer
{
/*============================================================================*/
/* LOCAL VARS AND FUNCS                                                       */
/*============================================================================*/

/*============================================================================*/
/* CONSTRUCTORS / DESTRUCTOR                                                  */
/*============================================================================*/

EmgDataContainer::EmgDataContainer():
    m_vecData{}
{
    m_vecData.reserve(0x1000);
}

/*============================================================================*/
/* IMPLEMENTATION                                                             */
/*============================================================================*/

void EmgDataContainer::Add(EmgData const &rEmg)
{
    if ((m_vecData.capacity() - m_vecData.size()) < 0x400)
    {
        m_vecData.reserve(m_vecData.size() + 0x1000);
    }
    m_vecData.push_back(rEmg);
    emit EmgDataAdded();
}

std::ostream &operator<<(std::ostream &os, EmgDataContainer const &emgData)
{
    using namespace std;
    os << "timestep; sensor0; sensor1; sensor2; sensor3; sensor4; sensor5; sensor6; sensor7;\n";
    auto vecCleaned = std::vector<EmgData>{begin(emgData), end(emgData)};
    std::sort(begin(vecCleaned), end(vecCleaned));
    auto const itBeginErase = std::unique(begin(vecCleaned), end(vecCleaned));
    vecCleaned.erase(itBeginErase, end(vecCleaned));
    for (auto const &emg : vecCleaned)
    {
        os << emg.m_uiTimestamp << "; ";
        for (int i = 0; i < 8; ++i)
        {
            os << +emg[i] << "; ";
        }
        os << '\n';
    }
    return os;
}

std::istream &operator>>(std::istream &is, EmgDataContainer &emgContainer)
{
    std::string strLine{};
    char cIgnoreDelim;
    // ignore header file for now
    if (!std::getline(is, strLine))
    {
        return is;
    }
    while (std::getline(is, strLine))
    {
        std::istringstream ss{strLine};
        std::uint64_t uiTimestamp{};
        int tmp[8];
        std::int8_t emg[8];
        ss >> uiTimestamp >> cIgnoreDelim
            >> tmp[0] >> cIgnoreDelim >> tmp[1] >> cIgnoreDelim
            >> tmp[2] >> cIgnoreDelim >> tmp[3] >> cIgnoreDelim
            >> tmp[4] >> cIgnoreDelim >> tmp[5] >> cIgnoreDelim 
            >> tmp[6] >> cIgnoreDelim >> tmp[7] >> cIgnoreDelim;
        std::copy(std::begin(tmp), std::end(tmp), std::begin(emg));
        emgContainer.Add(EmgData{uiTimestamp, emg});
    }
    return is;
}

} // namespace MyoVisualizer
